package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.io.Serial;

@Entity(name = "AppointmentNote")
@Table(name = "appointment_note")
public class AppointmentNoteEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 3509431080329714032L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "appointment_id", foreignKey = @ForeignKey(name = "FK_appointmentnote_appointment"))
    public AppointmentEntity appointment;
    public String text;

}
