package de.christophbrill.fairplanner.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class RegionGroupZipEntity {

    @Column(name = "zip_from")
    public String from;
    @Column(name = "zip_to")
    public String to;

}
