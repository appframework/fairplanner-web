package de.christophbrill.fairplanner.persistence.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.io.Serial;

@Entity(name = "Cost")
@Table(name = "cost")
public class CostEntity extends AbstractMonetaryAmountEntity {

    @Serial
    private static final long serialVersionUID = -6362723249808133121L;

}
