package de.christophbrill.fairplanner.persistence.model;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;

import de.christophbrill.appframework.persistence.model.DbObject;

/**
 * A quotation for a customer (likely based on an offer of a fair, but not necessarly)
 */
@Entity(name = "Quatation")
@Table(name = "quotation")
public class QuotationEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -4103937339404311341L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    public CustomerEntity customer;
    @OneToMany(mappedBy = "quotation", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<QuotationProductEntity> positions = new ArrayList<>(0);

}
