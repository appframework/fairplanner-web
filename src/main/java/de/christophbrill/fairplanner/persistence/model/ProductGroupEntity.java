package de.christophbrill.fairplanner.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.christophbrill.appframework.persistence.model.DbObject;
import org.apache.commons.collections4.CollectionUtils;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Entity(name = "ProductGroup")
@Table(name = "product_group")
public class ProductGroupEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -5758359728279094138L;

    public String name;
    public String color;
    @ManyToMany(mappedBy = "productGroups")
    @JsonIgnore
    public List<ProductEntity> products = new ArrayList<>(0);

    public static List<ProductGroupEntity> findByIds(Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return ProductGroupEntity.list("id in ?1", ids);
    }

}
