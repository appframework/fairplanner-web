package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import org.locationtech.jts.geom.Polygon;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.io.Serial;

@Entity(name = "Region")
@Table(name = "region")
public class RegionEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 4527722539841001618L;

    public String zip;
    public Polygon geom;

}
