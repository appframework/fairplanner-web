package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * An offer available to everyone during a fair.
 */
@Entity(name = "Offer")
@Table(name = "offer")
public class OfferEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 7910562382876952613L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fair_id")
    public FairEntity fair;
    @ManyToMany
    @JoinTable(name = "offer_productgroup", joinColumns = @JoinColumn(name = "offer_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "productgroup_id", referencedColumnName = "id"))
    @OrderBy("name")
    public List<ProductGroupEntity> productGroups = new ArrayList<>(0);
    @ManyToMany
    @JoinTable(name = "offer_product", joinColumns = @JoinColumn(name = "offer_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"))
    @OrderBy("name")
    public List<ProductEntity> products = new ArrayList<>(0);
    @Min(0)
    @Max(1)
    public double reduction;

}
