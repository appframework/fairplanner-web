package de.christophbrill.fairplanner.persistence.model;

import jakarta.annotation.Nonnull;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

import de.christophbrill.fairplanner.ui.dto.Contact.Type;

import java.util.Comparator;

@Embeddable
public class ContactEntity implements Comparable<ContactEntity> {

    @Enumerated(EnumType.STRING)
    public Type type;
    public String value;

    public ContactEntity() {
    }

    public ContactEntity(Type type, String value) {
        this.type = type;
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    private static final Comparator<ContactEntity> COMPARATOR = Comparator
            .comparing(ContactEntity::getType, Comparator.nullsFirst(Type::compareTo))
            .thenComparing(ContactEntity::getValue, Comparator.nullsFirst(String::compareToIgnoreCase));

    @Override
    public int compareTo(@Nonnull ContactEntity o) {
        return COMPARATOR.compare(this, o);
    }
}
