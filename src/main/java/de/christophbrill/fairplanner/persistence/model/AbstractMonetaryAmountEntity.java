package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.fairplanner.ui.dto.Interval;

import jakarta.persistence.*;

import java.io.Serial;

@MappedSuperclass
public class AbstractMonetaryAmountEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -5565139805994162628L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    public ProductEntity product;
    @Enumerated(EnumType.STRING)
    public Interval interval;
    public double amount;

}
