package de.christophbrill.fairplanner.persistence.model;

import jakarta.persistence.*;

import java.io.Serial;

@Entity(name = "Revenue")
@Table(name = "revenue")
public class RevenueEntity extends AbstractMonetaryAmountEntity {

    @Serial
    private static final long serialVersionUID = -8500420938995754244L;

}
