package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

import java.io.Serial;

@Entity(name = "QuotationProduct")
@Table(name = "quotation_product")
public class QuotationProductEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -3286791901005480863L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "quotation_id")
    public QuotationEntity quotation;
    @ManyToOne
    @JoinColumn(name = "product_id")
    public ProductEntity product;
    @Min(0)
    @Max(1)
    public double reduction;

}
