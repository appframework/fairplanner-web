package de.christophbrill.fairplanner.persistence.model;

import jakarta.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class IdentifierEntity implements Comparable<IdentifierEntity> {

    public String type;
    public String value;

    public IdentifierEntity() {
    }

    public IdentifierEntity(String type, String value) {
        this.type = type;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentifierEntity that = (IdentifierEntity) o;
        return Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public int compareTo(IdentifierEntity o) {
        return type.compareTo(o.type);
    }
}
