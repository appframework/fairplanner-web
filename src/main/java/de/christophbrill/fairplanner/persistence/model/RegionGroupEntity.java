package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import jakarta.annotation.Nullable;
import jakarta.persistence.Column;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "RegionGroup")
@Table(name = "regiongroup")
public class RegionGroupEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 5283996726992525710L;

    public String name;
    public String color;
    @ElementCollection
    @CollectionTable(name = "regiongroup_zip", joinColumns = @JoinColumn(name = "regiongroup_id"))
    @Cascade(CascadeType.ALL)
    @OrderBy("from")
    public List<RegionGroupZipEntity> zips = new ArrayList<>(0);
    @ElementCollection
    @CollectionTable(name = "regiongroup_alias", joinColumns = @JoinColumn(name = "regiongroup_id"))
    @Column(name = "alias")
    public List<String> alias = new ArrayList<>(0);

    @Nullable
    public static RegionGroupEntity findByNameOrAlias(String name) {
        if (name == null) {
            return null;
        }
        try {
            return getEntityManager().createQuery("select rg from RegionGroup rg " +
                            "where rg.name = :name " +
                            "or :name in elements(rg.alias)", RegionGroupEntity.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException | NonUniqueResultException e) {
            return null;
        }
    }
}
