package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.appframework.persistence.model.UserEntity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.io.Serial;
import java.time.LocalDateTime;
import java.util.List;

@Entity(name = "Appointment")
@Table(name = "appointment")
public class AppointmentEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -1502123807120703263L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id", foreignKey = @ForeignKey(name = "FK_appointment_customer"))
    public CustomerEntity customer;
    public LocalDateTime start;
    public LocalDateTime end;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_appointment_user"))
    public UserEntity user;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fair_id", foreignKey = @ForeignKey(name = "FK_appointment_fair"))
    public FairEntity fair;
    @OneToMany(mappedBy = "appointment")
    public List<AppointmentNoteEntity> notes;

}
