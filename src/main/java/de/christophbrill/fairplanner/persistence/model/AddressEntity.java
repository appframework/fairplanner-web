package de.christophbrill.fairplanner.persistence.model;

import jakarta.persistence.Embeddable;

@Embeddable
public class AddressEntity {

    public String street;
    public String zip;
    public String city;
    public Double lat;
    public Double lon;

}
