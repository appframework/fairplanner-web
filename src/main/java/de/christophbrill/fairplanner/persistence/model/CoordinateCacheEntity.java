package de.christophbrill.fairplanner.persistence.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import jakarta.persistence.*;

@Entity(name = "CoordinateCache")
@Table(name = "coordinate_cache")
public class CoordinateCacheEntity extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String query;
    public Double lat;
    public Double lon;

}
