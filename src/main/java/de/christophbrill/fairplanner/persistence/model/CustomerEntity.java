package de.christophbrill.fairplanner.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import jakarta.annotation.Nullable;
import org.hibernate.annotations.BatchSize;

import jakarta.annotation.Nonnull;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NoResultException;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

import java.io.Serial;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Entity(name = "Customer")
@Table(name = "customer")
public class CustomerEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 498316428256595031L;

    @Nonnull
    @Column(nullable = false)
    public String name;
    @Embedded
    public AddressEntity address;
    @BatchSize(size = 10)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "identifier", joinColumns = @JoinColumn(name = "customer_id"))
    @OrderBy("type")
    public Set<IdentifierEntity> identifiers = new TreeSet<>();
    @BatchSize(size = 10)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "contact", joinColumns = @JoinColumn(name = "customer_id"))
    @OrderBy("type")
    public Set<ContactEntity> contactdata = new TreeSet<>();

    @Nullable
    public static CustomerEntity findByIdentifier(String type, String value) {
        try {
            return getEntityManager().createQuery("select c from Customer c " +
                            "inner join c.identifiers i " +
                            "where i.type = :type " +
                            "and i.value = :value", CustomerEntity.class)
                    .setParameter("type", type)
                    .setParameter("value", value)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public static List<String> getIdentifiers() {
        return getEntityManager()
                .createQuery("select distinct i.type from Customer c " +
                        "inner join c.identifiers i " +
                        "order by i.type", String.class)
                .getResultList();

    }
}
