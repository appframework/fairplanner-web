package de.christophbrill.fairplanner.persistence.model;

import java.io.Serial;
import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "Fair")
@Table(name = "fair")
public class FairEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 6568036505804670523L;

    public String name;
    public LocalDate start;
    @Column(name = "`end`")
    public LocalDate end;

}
