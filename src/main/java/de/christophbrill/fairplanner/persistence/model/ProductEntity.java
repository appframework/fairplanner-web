package de.christophbrill.fairplanner.persistence.model;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;

@Entity(name = "Product")
@Table(name = "product")
public class ProductEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -4838874725511418636L;

    public String name;
    @ManyToMany
    @JoinTable(name = "product_productgroup", joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "productgroup_id", referencedColumnName = "id"))
    @OrderBy("name")
    public List<ProductGroupEntity> productGroups = new ArrayList<>(0);
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<CostEntity> costs = new ArrayList<>(0);
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<RevenueEntity> revenues = new ArrayList<>(0);

}
