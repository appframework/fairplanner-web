package de.christophbrill.fairplanner.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.spatial.criteria.SpatialCriteriaBuilder;
import org.hibernate.spatial.criteria.internal.JTSSpatialCriteriaBuilderImpl;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.PrecisionModel;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.fairplanner.persistence.model.RegionEntity;
import de.christophbrill.fairplanner.persistence.model.RegionEntity_;
import de.christophbrill.fairplanner.ui.dto.Bounds;
import de.christophbrill.fairplanner.ui.dto.LatLng;

public class RegionSelector extends AbstractResourceSelector<RegionEntity> {

    private String search;
    private Bounds bounds;

    public RegionSelector(EntityManager em) {
        super(em);
    }

    @Override
    protected CriteriaBuilder createCriteriaBuilder() {
        return new JTSSpatialCriteriaBuilderImpl((HibernateCriteriaBuilder) super.createCriteriaBuilder());
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<RegionEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {

        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(RegionEntity_.zip), '%' + search + '%'));
        }

        if (bounds != null) {
            GeometryFactory factory = new GeometryFactory(new PrecisionModel(PrecisionModel.FIXED), 4326);
            LatLng northEast = bounds.northEast;
            LatLng southWest = bounds.southWest;
            Coordinate[] coordinates = { new Coordinate(northEast.lat, northEast.lng, 0),
                    new Coordinate(southWest.lat, northEast.lng, 0),
                    new Coordinate(southWest.lat, southWest.lng, 0),
                    new Coordinate(northEast.lat, southWest.lng, 0),
                    new Coordinate(northEast.lat, northEast.lng, 0) };
            LinearRing polygon = factory.createLinearRing(coordinates);
            Polygon po = factory.createPolygon(polygon, null);
            predicates.add(((SpatialCriteriaBuilder<Polygon>) builder).intersects(from.get("geom"), po));
        }

        return predicates;
    }

    @Override
    public RegionSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<RegionEntity> getEntityClass() {
        return RegionEntity.class;
    }

    public RegionSelector intersects(Bounds bounds) {
        this.bounds = bounds;
        return this;
    }

}
