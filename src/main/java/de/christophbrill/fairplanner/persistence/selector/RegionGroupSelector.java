package de.christophbrill.fairplanner.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity_;

public class RegionGroupSelector extends AbstractResourceSelector<RegionGroupEntity> {

    private String search;
    private String name;

    public RegionGroupSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<RegionGroupEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.or(
                    builder.like(builder.lower(from.get(RegionGroupEntity_.name)), '%' + search.toLowerCase() + '%'),
                    builder.like(builder.lower(from.get(RegionGroupEntity_.color)), '%' + search.toLowerCase() + '%')
            ));
        }

        if (StringUtils.isNotEmpty(name)) {
            predicates.add(builder.like(builder.lower(from.get(RegionGroupEntity_.name)), '%' + name.toLowerCase() + '%'));
        }

        return predicates;
    }

    @Override
    public RegionGroupSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    public RegionGroupSelector withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    @Nonnull
    protected Class<RegionGroupEntity> getEntityClass() {
        return RegionGroupEntity.class;
    }

}
