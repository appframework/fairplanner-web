package de.christophbrill.fairplanner.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import de.christophbrill.fairplanner.persistence.model.FairEntity_;

public class FairSelector extends AbstractResourceSelector<FairEntity> {

    private String search;

    public FairSelector(EntityManager em) {
        super(em);
    }

    @Nonnull
    @Override
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<FairEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(FairEntity_.name), '%' + search + '%'));
        }

        return predicates;
    }

    @Override
    public FairSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Nonnull
    @Override
    protected Class<FairEntity> getEntityClass() {
        return FairEntity.class;
    }

}
