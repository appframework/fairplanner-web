package de.christophbrill.fairplanner.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity_;
import de.christophbrill.fairplanner.persistence.model.QuotationEntity;
import de.christophbrill.fairplanner.persistence.model.QuotationEntity_;

public class QuotationSelector extends AbstractResourceSelector<QuotationEntity> {

    private String search;

    public QuotationSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<QuotationEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(QuotationEntity_.customer).get(CustomerEntity_.name), '%' + search + '%'));
        }

        return predicates;
    }

    @Override
    public QuotationSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<QuotationEntity> getEntityClass() {
        return QuotationEntity.class;
    }

}
