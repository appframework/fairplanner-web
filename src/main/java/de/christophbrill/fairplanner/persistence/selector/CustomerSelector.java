package de.christophbrill.fairplanner.persistence.selector;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.SetJoin;

import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.fairplanner.persistence.model.AddressEntity_;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity_;
import de.christophbrill.fairplanner.persistence.model.IdentifierEntity;
import de.christophbrill.fairplanner.persistence.model.IdentifierEntity_;
import de.christophbrill.fairplanner.ui.dto.Bounds;

public class CustomerSelector extends AbstractResourceSelector<CustomerEntity> {

    private String search;
    private Bounds bounds;
    private RegionGroupEntity regionGroup;

    private static final Pattern PATTERN_SEARCH = Pattern.compile("(\\w+):\\s*(\"([^\"]+)\"|([^\\s]+))");

    public CustomerSelector(EntityManager em) {
        super(em);
    }

    @Nonnull
    @Override
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<CustomerEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            if (search.contains(":")) {
                evaluateSearch(search, predicates, builder, from, criteriaQuery);
            } else {
                applySearch(search, predicates, builder, from, criteriaQuery);
            }
        }

        if (bounds != null) {
            predicates.add(
                builder.and(
                    builder.ge(from.get(CustomerEntity_.address).get(AddressEntity_.lon), bounds.southWest.lng),
                    builder.le(from.get(CustomerEntity_.address).get(AddressEntity_.lon), bounds.northEast.lng),
                    builder.ge(from.get(CustomerEntity_.address).get(AddressEntity_.lat), bounds.southWest.lat),
                    builder.le(from.get(CustomerEntity_.address).get(AddressEntity_.lat), bounds.northEast.lat)
                )
            );
        }

        if (regionGroup != null) {
            SetJoin<CustomerEntity, IdentifierEntity> identifierJoin = from.join(CustomerEntity_.identifiers);
            predicates.add(builder.and(
                    builder.equal(identifierJoin.get(IdentifierEntity_.value), regionGroup.name),
                    builder.equal(identifierJoin.get(IdentifierEntity_.type), "regiongroup")
            ));
        }

        return predicates;
    }

    private static void applySearch(String search, List<Predicate> predicates, @Nonnull CriteriaBuilder builder,
            @Nonnull Root<CustomerEntity> from, @Nonnull CriteriaQuery<?> criteriaQuery) {
        criteriaQuery.distinct(true);
        String likePattern = '%' + search + '%';
        predicates.add(
            builder.or(
                builder.like(from.join(CustomerEntity_.identifiers, JoinType.LEFT).get(IdentifierEntity_.value), likePattern),
                builder.like(from.join(CustomerEntity_.address).get(AddressEntity_.zip), likePattern),
                builder.like(from.join(CustomerEntity_.address).get(AddressEntity_.city), likePattern),
                builder.like(from.join(CustomerEntity_.address).get(AddressEntity_.street), likePattern),
                builder.like(from.get(CustomerEntity_.name), likePattern)
            )
        );
    }

    static void evaluateSearch(String search, List<Predicate> predicates, CriteriaBuilder builder, Root<CustomerEntity> from, CriteriaQuery<?> criteriaQuery) {
        Matcher matcher = PATTERN_SEARCH.matcher(search);
        boolean found = false;
        int lastIndex = -1;
        while (matcher.find()) {
            found = true;
            String type = matcher.group(1);
            String term = matcher.group(3);
            if (term == null) {
                term = matcher.group(4);
            }
            term = term.toLowerCase();
            if (lastIndex + 1 < matcher.start()) {
                applySearch(search.substring(lastIndex + 1, matcher.start()).trim(), predicates, builder, from, criteriaQuery);
            }
            switch (type) {
            case "name":
                predicates.add(termToPredicate(builder, term, from.get(CustomerEntity_.name)));
                break;
            case "city":
                predicates.add(termToPredicate(builder, term, from.join(CustomerEntity_.address).get(AddressEntity_.city)));
                break;
            case "zip":
                predicates.add(termToPredicate(builder, term, from.join(CustomerEntity_.address).get(AddressEntity_.zip)));
                break;
            case "street":
                predicates.add(termToPredicate(builder, term, from.join(CustomerEntity_.address).get(AddressEntity_.street)));
                break;
            default:
                SetJoin<CustomerEntity, IdentifierEntity> identifierJoin = from.join(CustomerEntity_.identifiers);
                predicates.add(builder.and(
                        builder.equal(identifierJoin.get(IdentifierEntity_.type), type),
                        termToPredicate(builder, term, identifierJoin.get(IdentifierEntity_.value))
                ));
            }
            lastIndex = matcher.end();
        }
        if (lastIndex + 1 < search.length()) {
            applySearch(search.substring(lastIndex + 1), predicates, builder, from, criteriaQuery);
            found = true;
        }
        if (!found) {
            applySearch(search, predicates, builder, from, criteriaQuery);
        }
    }

    private static Predicate termToPredicate(CriteriaBuilder builder, String term,
            Expression<String> expression) {
        if (term.startsWith("!")) {
            return builder.notLike(builder.lower(expression), '%' + term.substring(1) + '%');
        }
        return builder.like(builder.lower(expression), '%' + term + '%');
    }

    @Override
    public CustomerSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<CustomerEntity> getEntityClass() {
        return CustomerEntity.class;
    }

    public CustomerSelector withBounds(Bounds bounds) {
        this.bounds = bounds;
        return this;
    }

    public CustomerSelector withRegionGroup(RegionGroupEntity regionGroup) {
        this.regionGroup = regionGroup;
        return this;
    }
}
