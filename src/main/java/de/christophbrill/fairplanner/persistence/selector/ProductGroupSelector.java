package de.christophbrill.fairplanner.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity_;

public class ProductGroupSelector extends AbstractResourceSelector<ProductGroupEntity> {

    private String search;

    public ProductGroupSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<ProductGroupEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(ProductGroupEntity_.name), '%' + search + '%'));
        }

        return predicates;
    }

    @Override
    public ProductGroupSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<ProductGroupEntity> getEntityClass() {
        return ProductGroupEntity.class;
    }

}
