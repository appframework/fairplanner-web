package de.christophbrill.fairplanner.ui.dto;

public enum Interval {
    SINGLE, QUARTERLY, MONTHLY
}
