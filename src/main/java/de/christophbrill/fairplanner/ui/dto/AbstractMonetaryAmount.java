package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class AbstractMonetaryAmount extends AbstractDto {

    public Interval interval;
    public double amount;

}
