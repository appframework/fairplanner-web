package de.christophbrill.fairplanner.ui.dto;

public class Bounds {

    public LatLng northEast;
    public LatLng southWest;
    public Integer zoom;

}
