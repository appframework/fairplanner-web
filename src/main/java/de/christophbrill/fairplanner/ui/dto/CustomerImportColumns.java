package de.christophbrill.fairplanner.ui.dto;

import java.util.List;
import java.util.Map;

import de.christophbrill.fairplanner.ui.dto.Contact.Type;

public class CustomerImportColumns {

    public List<Integer> name;
    public List<Integer> street;
    public List<Integer> zip;
    public List<Integer> city;
    public Map<Type, List<Integer>> contact;
    public Map<String, List<Integer>> identifiers;

}