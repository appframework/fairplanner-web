package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

import java.util.List;

public class Offer extends AbstractDto {

    public Long fairId;
    public List<Product> products;
    public List<ProductGroup> productGroups;
    public double reduction;

}
