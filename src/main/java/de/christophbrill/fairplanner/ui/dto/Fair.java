package de.christophbrill.fairplanner.ui.dto;

import java.time.LocalDate;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Fair extends AbstractDto {

    public String name;
    public LocalDate start;
    public LocalDate end;

}
