package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class QuotationProduct extends AbstractDto {

    public Long productId;
    public double reduction;

}
