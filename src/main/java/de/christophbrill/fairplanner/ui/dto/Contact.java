package de.christophbrill.fairplanner.ui.dto;

public class Contact {

    public enum Type {
        LANDLINE,
        MOBILE,
        FAX,
        EMAIL,
        WEBSITE
    }

    public Type type;
    public String value;

}
