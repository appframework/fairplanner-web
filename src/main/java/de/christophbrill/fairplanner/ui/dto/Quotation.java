package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

import java.util.List;

public class Quotation extends AbstractDto {

    public Long customerId;
    public List<QuotationProduct> positions;

}
