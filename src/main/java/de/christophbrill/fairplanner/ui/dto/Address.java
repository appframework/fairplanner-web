package de.christophbrill.fairplanner.ui.dto;

public class Address {

    public String street;
    public String zip;
    public String city;
    public Double lat;
    public Double lon;

}
