package de.christophbrill.fairplanner.ui.dto;

import java.util.List;

public class ImportConfiguration {

    public List<Column> columns;
    public String configuration;
    public int identifierColumn;
    public String identifierType;
    public int sheet;

}
