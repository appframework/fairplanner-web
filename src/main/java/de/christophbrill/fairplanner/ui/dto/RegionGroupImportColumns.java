package de.christophbrill.fairplanner.ui.dto;

import java.util.List;

public class RegionGroupImportColumns {

    public List<Integer> from;
    public List<Integer> to;

}