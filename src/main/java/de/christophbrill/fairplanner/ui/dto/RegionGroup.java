package de.christophbrill.fairplanner.ui.dto;

import java.util.List;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class RegionGroup extends AbstractDto {

    public String name;
    public String color;
    public List<RegionGroupZip> zips;
    public List<String> alias;

}
