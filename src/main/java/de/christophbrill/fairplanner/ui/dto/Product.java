package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;
import java.util.List;

public class Product extends AbstractDto {

    public String name;
    public List<Long> productGroupIds;
    public List<Cost> costs;
    public List<Revenue> revenues;

}
