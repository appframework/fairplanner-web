package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

import java.util.List;

public class Customer extends AbstractDto {

    public String name;
    public Address address;
    public List<Identifier> identifiers;
    public List<Contact> contactdata;

}
