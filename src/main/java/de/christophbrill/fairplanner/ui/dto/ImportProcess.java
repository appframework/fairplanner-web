package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class ImportProcess extends AbstractDto {

    public String filename;
    public String columns;
    public int identifierColumn;
    public String identifierType;

}
