package de.christophbrill.fairplanner.ui.dto;

import java.util.List;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Region extends AbstractDto {

    public String zip;
    public List<LatLng> geom;

}
