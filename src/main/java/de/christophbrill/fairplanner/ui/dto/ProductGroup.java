package de.christophbrill.fairplanner.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class ProductGroup extends AbstractDto {

    public String name;
    public String color;

}
