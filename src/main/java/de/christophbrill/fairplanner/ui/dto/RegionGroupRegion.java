package de.christophbrill.fairplanner.ui.dto;

public class RegionGroupRegion {
    public RegionGroup group;
    public Region region;
    public RegionGroupRegion(RegionGroup group, Region region) {
        this.group = group;
        this.region = region;
    }
}
