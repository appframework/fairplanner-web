package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.OfferEntity;
import de.christophbrill.fairplanner.persistence.selector.OfferSelector;
import de.christophbrill.fairplanner.ui.dto.Offer;
import de.christophbrill.fairplanner.util.mappers.OfferMapper;

import jakarta.ws.rs.Path;

@Path("/offer")
public class OfferService extends AbstractResourceService<Offer, OfferEntity> {

    @Override
    protected Class<OfferEntity> getEntityClass() {
        return OfferEntity.class;
    }

    @Override
    protected OfferSelector getSelector() {
        return new OfferSelector(em);
    }

    @Override
    protected OfferMapper getMapper() {
        return OfferMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_OFFERS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_OFFERS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_OFFERS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_OFFERS";
    }

}
