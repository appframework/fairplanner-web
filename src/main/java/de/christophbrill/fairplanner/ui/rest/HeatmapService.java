package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import de.christophbrill.fairplanner.persistence.selector.CustomerSelector;
import de.christophbrill.fairplanner.ui.dto.Bounds;
import jakarta.persistence.EntityManager;
import org.apache.commons.lang3.ObjectUtils;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

@Path("/heatmap")
public class HeatmapService {

    @Inject
    EntityManager em;

    public static class Layer {
        private String name;
        private List<Object[]> coordinates;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Object[]> getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(List<Object[]> coordinates) {
            this.coordinates = coordinates;
        }
    }

    @POST
    @Path("within")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Layer> getLayersWithin(Bounds bounds) {
        // FIXME: disabled, for no obvious reason this will always return false
        /*if (!subject.hasRole(Permission.SHOW_REGIONGROUPS.toString())) {
            return Collections.emptyList();
        }*/

        var regionGroups = RegionGroupEntity.<RegionGroupEntity>listAll();

        List<Layer> layers = new ArrayList<>();
        for (RegionGroupEntity regionGroup : regionGroups) {
            Layer layer = new Layer();
            layer.setName(regionGroup.name);
            layer.setCoordinates(new ArrayList<>());

            new CustomerSelector(em)
                    .withRegionGroup(regionGroup)
                    .withBounds(bounds)
                    .findAll()
                    .forEach(customer -> layer.getCoordinates().add(new Object[]{
                            ObjectUtils.defaultIfNull(customer.address.lat, 0.0d),
                            ObjectUtils.defaultIfNull(customer.address.lon, 0.0d),
                            "100"
                    }));

            layers.add(layer);

        }

        return layers;

    }

}
