package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.CostEntity;
import de.christophbrill.fairplanner.persistence.model.ProductEntity;
import de.christophbrill.fairplanner.persistence.model.RevenueEntity;
import de.christophbrill.fairplanner.persistence.selector.ProductSelector;
import de.christophbrill.fairplanner.ui.dto.Product;
import de.christophbrill.fairplanner.util.mappers.ProductMapper;

import jakarta.ws.rs.Path;

@Path("/product")
public class ProductService extends AbstractResourceService<Product, ProductEntity> {

    @Override
    protected Class<ProductEntity> getEntityClass() {
        return ProductEntity.class;
    }

    @Override
    protected ProductSelector getSelector() {
        return new ProductSelector(em);
    }

    @Override
    protected ProductMapper getMapper() {
        return ProductMapper.INSTANCE;
    }

    @Override
    protected ProductEntity mapCreate(Product product) {
        var entity = super.mapCreate(product);
        for (CostEntity c : entity.costs) {
            c.product = entity;
        }
        for (RevenueEntity r : entity.revenues) {
            r.product = entity;
        }
        return entity;
    }

    @Override
    protected ProductEntity mapUpdate(Product product) {
        var entity = super.mapUpdate(product);
        for (CostEntity c : entity.costs) {
            c.product = entity;
        }
        for (RevenueEntity r : entity.revenues) {
            r.product = entity;
        }
        return entity;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_PRODUCTS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_PRODUCTS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_PRODUCTS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_PRODUCTS";
    }
}
