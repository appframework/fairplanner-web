package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.QuotationEntity;
import de.christophbrill.fairplanner.persistence.selector.QuotationSelector;
import de.christophbrill.fairplanner.ui.dto.Quotation;
import de.christophbrill.fairplanner.util.mappers.QuotationMapper;

import jakarta.ws.rs.Path;

@Path("/quotation")
public class QuotationService extends AbstractResourceService<Quotation, QuotationEntity> {

    @Override
    protected Class<QuotationEntity> getEntityClass() {
        return QuotationEntity.class;
    }

    @Override
    protected QuotationSelector getSelector() {
        return new QuotationSelector(em);
    }

    @Override
    protected QuotationMapper getMapper() {
        return QuotationMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_QUOTATIONS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_QUOTATIONS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_QUOTATIONS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_QUOTATIONS";
    }

}
