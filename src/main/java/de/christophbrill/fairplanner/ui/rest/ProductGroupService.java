package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity;
import de.christophbrill.fairplanner.persistence.selector.ProductGroupSelector;

import de.christophbrill.fairplanner.ui.dto.ProductGroup;
import de.christophbrill.fairplanner.util.mappers.ProductGroupMapper;
import jakarta.ws.rs.Path;

@Path("/productgroup")
public class ProductGroupService extends AbstractResourceService<ProductGroup, ProductGroupEntity> {

    @Override
    protected Class<ProductGroupEntity> getEntityClass() {
        return ProductGroupEntity.class;
    }

    @Override
    protected ProductGroupSelector getSelector() {
        return new ProductGroupSelector(em);
    }

    @Override
    protected ProductGroupMapper getMapper() {
        return ProductGroupMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_PRODUCTGROUPS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_PRODUCTGROUPS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_PRODUCTGROUPS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_PRODUCTGROUPS";
    }

}
