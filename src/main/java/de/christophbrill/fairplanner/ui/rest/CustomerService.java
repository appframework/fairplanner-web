package de.christophbrill.fairplanner.ui.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.exceptions.NotAuthorizedException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.persistence.selector.CustomerSelector;
import de.christophbrill.fairplanner.ui.dto.Bounds;
import de.christophbrill.fairplanner.ui.dto.Customer;
import de.christophbrill.fairplanner.util.imports.CoordinateCacheUtil;
import de.christophbrill.fairplanner.util.mappers.CustomerMapper;
import jakarta.persistence.LockModeType;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PathParam;
import org.apache.commons.lang3.StringUtils;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.List;

@Path("/customer")
public class CustomerService extends AbstractResourceService<Customer, CustomerEntity> {

    @ConfigProperty(name = "quarkus.application.version")
    String version;

    @Override
    protected Class<CustomerEntity> getEntityClass() {
        return CustomerEntity.class;
    }

    @Override
    protected CustomerSelector getSelector() {
        return new CustomerSelector(em);
    }

    @Override
    protected CustomerMapper getMapper() {
        return CustomerMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_CUSTOMERS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_CUSTOMERS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_CUSTOMERS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_CUSTOMERS";
    }

    @Override
    protected CustomerEntity mapCreate(Customer t) {
        var entity = super.mapCreate(t);
        geocode(entity, entity.address.street, entity.address.zip, entity.address.city);
        return entity;
    }

    @Override
    protected CustomerEntity mapUpdate(Customer t) {
        var old = CustomerEntity.<CustomerEntity>findById(t.id, LockModeType.NONE);
        String streetBefore = old.address.street;
        String zipBefore = old.address.zip;
        String cityBefore = old.address.city;
        var entity = super.mapUpdate(t);
        geocode(entity, streetBefore, zipBefore, cityBefore);
        return entity;
    }

    private boolean geocode(CustomerEntity entity, String streetBefore, String zipBefore, String cityBefore) {
        if (!StringUtils.equals(streetBefore, entity.address.street)
                || !StringUtils.equals(zipBefore, entity.address.zip)
                || !StringUtils.equals(cityBefore, entity.address.city)) {
            var cache = CoordinateCacheUtil.evaluateCoordinate(new ObjectMapper(),
                    "https://nominatim.openstreetmap.org/search?", entity.address.street,
                    entity.address.zip, entity.address.city, version);
            entity.address.lat = cache.lat;
            entity.address.lon = cache.lon;
            return cache.lat != null;
        }
        return false;
    }

    @POST
    @Path("within")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> within(Bounds bounds) {
        var customers = getSelector()
                .withBounds(bounds)
                .withLimit(1000)
                .findAll();
        return getMapper()
                .mapEntitiesToDtos(customers);
    }

    @GET
    @Path("/{type}/{identifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public Customer getByIdentifier(@PathParam("type") String type,
                                    @PathParam("identifier") String identifier) {
        checkReadPermission();

        var entity = CustomerEntity.findByIdentifier(type, identifier);
        return getMapper().mapEntityToDto(entity);
    }

    @GET
    @Path("/geocode/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Double[] geocode(@PathParam("id") Long id) {
        checkReadPermission();

        CustomerEntity customer = CustomerEntity.findById(id);
        if (geocode(customer, null, null, null)) {
            customer.persist();
        }
        return new Double[]{customer.address.lat, customer.address.lon};
    }

}
