package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.RegionEntity;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import de.christophbrill.fairplanner.persistence.selector.RegionSelector;
import de.christophbrill.fairplanner.ui.dto.Bounds;
import de.christophbrill.fairplanner.ui.dto.LatLng;
import de.christophbrill.fairplanner.ui.dto.Region;
import de.christophbrill.fairplanner.ui.dto.RegionGroup;
import de.christophbrill.fairplanner.ui.dto.RegionGroupRegion;
import de.christophbrill.fairplanner.ui.dto.RegionGroupZip;
import de.christophbrill.fairplanner.util.mappers.RegionGroupMapper;
import de.christophbrill.fairplanner.util.mappers.RegionMapper;
import io.quarkus.logging.Log;
import jakarta.annotation.Nullable;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/region")
public class RegionService extends AbstractResourceService<Region, RegionEntity> {

    @Override
    protected Class<RegionEntity> getEntityClass() {
        return RegionEntity.class;
    }

    @Override
    protected RegionSelector getSelector() {
        return new RegionSelector(em);
    }

    @Override
    protected RegionMapper getMapper() {
        return RegionMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_REGIONS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_REGIONS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_REGIONS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_REGIONS";
    }

    @POST
    @Path("within")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<RegionGroupRegion> within(Bounds bounds) {
        var regions = getSelector().intersects(bounds).findAll();
        // TODO should have happened on insert ...
        Map<RegionGroup, List<Region>> result = new HashMap<>();
        for (RegionGroupEntity group : RegionGroupEntity.<RegionGroupEntity>listAll()) {
            RegionGroup dto = RegionGroupMapper.INSTANCE.mapEntityToDto(group);
            result.put(dto, new ArrayList<>());
        }
        List<Region> unassigneds = new ArrayList<>();
        outer:
        for (RegionEntity region : regions) {
            for (Map.Entry<RegionGroup, List<Region>> entry : result.entrySet()) {
                RegionGroup group = entry.getKey();
                for (RegionGroupZip zip : group.zips) {
                    if (zip.from.compareTo(region.zip) <= 0 && zip.to.compareTo(region.zip) >= 0) {
                        Region map = getMapper().mapEntityToDto(region);
                        entry.getValue().add(reduce(map, bounds.zoom));
                        continue outer;
                    }
                }
            }
            Log.debugv("Found zip code {0} which did not belong to any region group", region.zip);
            Region map = getMapper().mapEntityToDto(region);
            unassigneds.add(reduce(map, bounds.zoom));
        }
        if (!unassigneds.isEmpty()) {
            RegionGroup unassigned = new RegionGroup();
            unassigned.color = "#ff00ff";
            unassigned.name = "Unassigned";
            result.put(unassigned, unassigneds);
        }
        var dtos = new ArrayList<RegionGroupRegion>();
        for (var entry : result.entrySet()) {
            for (var region : entry.getValue()) {
                dtos.add(new RegionGroupRegion(entry.getKey(), region));
            }
        }
        return dtos;
    }

    public static List<LatLng> douglasPeucker(List<LatLng> points, int startIndex, int lastIndex, double epsilon) {
        double dmax = 0;
        int index = startIndex;

        for (int i = index + 1; i < lastIndex; ++i) {
            double d = pointLineDistance(points.get(i), points.get(startIndex), points.get(lastIndex));
            if (d > dmax) {
                index = i;
                dmax = d;
            }
        }

        if (dmax > epsilon) {
            List<LatLng> res1 = douglasPeucker(points, startIndex, index, epsilon);
            List<LatLng> res2 = douglasPeucker(points, index, lastIndex, epsilon);

            List<LatLng> finalRes = new ArrayList<>();
            for (int i = 0; i < res1.size() - 1; ++i) {
                finalRes.add(res1.get(i));
            }

            finalRes.addAll(res2);

            return finalRes;
        }
        return new ArrayList<>(Arrays.asList(points.get(startIndex), points.get(lastIndex)));
    }

    public static double pointLineDistance(LatLng point, LatLng start, LatLng end) {
        if (start.equals(end)) {
            return Math
                    .sqrt(Math.pow(point.lng - start.lng, 2) + Math.pow(point.lat - start.lat, 2));
        }

        double n = Math.abs((end.lng - start.lng) * (start.lat - point.lat)
                - (start.lng - point.lng) * (end.lat - start.lat));
        double d = Math.sqrt((end.lng - start.lng) * (end.lng - start.lng)
                + (end.lat - start.lat) * (end.lat - start.lat));

        return n / d;
    }

    private static Region reduce(Region region, Integer zoom) {
        double epsilon = determineEpsilon(zoom);
        if (epsilon == 0) {
            return region;
        }
        if (region.geom.size() < 2) {
            return region;
        }
        region.geom = douglasPeucker(region.geom, 0, region.geom.size() - 1, epsilon);
        return region;
    }

    private static double determineEpsilon(@Nullable Integer zoom) {

        if (zoom == null) {
            return 0;
        }

        return switch (zoom) {
            case 7 -> 0.03;
            case 8 -> 0.01;
            case 9 -> 0.001;
            case 10 -> 0.0001;
            default -> 0;
        };
    }

}
