package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.async.ProgressCache;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.NotAuthorizedException;
import de.christophbrill.fairplanner.persistence.model.ImportProcessEntity;
import de.christophbrill.fairplanner.persistence.model.Permission;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.fairplanner.util.imports.RegionImport;
import de.christophbrill.fairplanner.util.imports.XlsxImportTools;
import io.quarkus.logging.Log;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.transaction.UserTransaction;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

@Path("/regionimport")
public class RegionImportService {

    @Inject
    UserTransaction transaction;
    @Inject
    ManagedExecutor executor;
    @Inject
    protected SecurityIdentity identity;

    @Inject
    RegionImport regionImport;
    @Inject
    ProgressCache progressCache;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String upload(@RestForm String filename,
                         @RestForm("file") FileUpload file,
                         @RestForm boolean removeExisting,
                         @RestForm String include) {
        if (!identity.hasRole(Permission.ADMIN_REGIONS.name())) {
            throw new NotAuthorizedException();
        }

        Progress<ImportResult> progress = progressCache.createProgress();

        ImportProcessEntity importProcess = XlsxImportTools.startProcessImport(transaction, filename, "", 0, null, progress);
        if (importProcess == null) {
            return progress.key;
        }

        Uni.createFrom()
                .item(() -> progress.key)
                .invoke(uuid -> regionImport.runImport(filename, removeExisting, file, include, progress))
                .emitOn(executor)
                .subscribe()
                .with(done -> {
                    progress.completed = true;
                    Log.info("Completed import");
                }, e -> {
                    progress.completed = true;
                    progress.success = false;
                    progress.message = e.getMessage();
                    Log.error(e.getMessage(), e);
                });
        Log.infov("Import started {0}", progress.key);

        return progress.key;
    }

}
