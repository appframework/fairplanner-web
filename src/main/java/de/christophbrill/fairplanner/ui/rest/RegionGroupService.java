package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import de.christophbrill.fairplanner.persistence.selector.RegionGroupSelector;

import de.christophbrill.fairplanner.ui.dto.RegionGroup;
import de.christophbrill.fairplanner.util.mappers.RegionGroupMapper;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/regiongroup")
public class RegionGroupService extends AbstractResourceService<RegionGroup, RegionGroupEntity> {

    @Override
    protected Class<RegionGroupEntity> getEntityClass() {
        return RegionGroupEntity.class;
    }

    @Override
    protected RegionGroupSelector getSelector() {
        return new RegionGroupSelector(em);
    }

    @Override
    protected RegionGroupMapper getMapper() {
        return RegionGroupMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_REGIONGROUPS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_REGIONGROUPS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_REGIONGROUPS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_REGIONGROUPS";
    }

    @GET
    @Path("default.svg")
    @Produces("image/svg+xml")
    public String getDefaultIcon() {
        return "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" height=\"41\" width=\"25\">" +
                " <rect style=\"fill:#ffffff\" width=\"12.625\" height=\"14.5\" x=\"6.3289919\" y=\"5.2859039\" />" +
                " <path d=\"m 12.550781,41 9.75781,-18.828126 c 1.129144,-2.437806 2.791012,-5.827897 2.791012,-8.982423 0,-6.4946379 -5.615394,-12.4160155 -12.505856,-12.4160145 C 5.7032848,0.7734375 0,6.6895897 0,13.189453 c 0,2.984432 1.6103852,6.53046 2.7470703,8.982422 z M 12.59375,9.0273438 c 2.2855,-2e-7 4.128906,1.8703062 4.128906,4.1601562 0,2.28985 -1.839083,4.12935 -4.128906,4.128906 C 10.303927,17.315962 8.4648438,15.477907 8.4648438,13.1875 8.4648439,10.897093 10.30825,9.027344 12.59375,9.0273438 Z\" style=\"fill:#333333\" />" +
                " <path d=\"m 12.594,1.3228953 c -6.573,0 -12.04399999,5.691 -12.04399999,11.8659997 0,2.778 1.56399999,6.308 2.69399999,8.746 l 9.306,17.872 9.262,-17.872 c 1.13,-2.438 2.738,-5.791 2.738,-8.746 0,-6.1749997 -5.383,-11.8659997 -11.956,-11.8659997 z m 0,7.155 c 2.584,0.017 4.679,2.1219997 4.679,4.7099997 0,2.588 -2.095,4.663 -4.679,4.679 -2.584,-0.017 -4.679,-2.09 -4.679,-4.679 0,-2.588 2.095,-4.6929997 4.679,-4.7099997 z\" style=\"fill:none;stroke:#000000;stroke-width:1.1;stroke-linecap:round;stroke-opacity:0.303\" />" +
                " <path d=\"m 12.55,37.429895 8.2655,-15.9845 c 1.1505,-2.4635 2.6405,-5.7215 2.6405,-8.2655 0,-5.5379997 -4.931,-10.7499997 -10.875,-10.7499997 -5.944,0 -10.938,5.219 -10.938,10.7499997 0,2.359 1.443,5.832 2.5785,8.2655 z M 12.581,7.3988953 c 3.168066,5e-4 5.781,2.6009999 5.781,5.7809997 0,3.18 -2.612937,5.781 -5.781,5.781 -3.1680631,0 -5.75,-2.6095 -5.75,-5.781 0,-3.1715 2.5819337,-5.7814997 5.75,-5.7809997 z\" style=\"fill:none;stroke:#ffffff;stroke-width:1.1;stroke-linecap:round;stroke-opacity:0.122\" />" +
                "  </svg>";
    }

    @GET
    @Path("{id}.svg")
    @Produces("image/svg+xml")
    public String getIcon(@PathParam("id") String idOrName) {
        RegionGroupEntity group = null;
        if (idOrName.matches("\\d")) {
            var id = Long.valueOf(idOrName);
            group = RegionGroupEntity.findById(id);
        } else {
            group = RegionGroupEntity.findByNameOrAlias(idOrName);
        }
        if (group == null) {
            return getDefaultIcon();
        }
        return "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" height=\"41\" width=\"25\">" +
                " <rect style=\"fill:#ffffff\" width=\"12.625\" height=\"14.5\" x=\"6.3289919\" y=\"5.2859039\" />" +
                " <path d=\"m 12.550781,41 9.75781,-18.828126 c 1.129144,-2.437806 2.791012,-5.827897 2.791012,-8.982423 0,-6.4946379 -5.615394,-12.4160155 -12.505856,-12.4160145 C 5.7032848,0.7734375 0,6.6895897 0,13.189453 c 0,2.984432 1.6103852,6.53046 2.7470703,8.982422 z M 12.59375,9.0273438 c 2.2855,-2e-7 4.128906,1.8703062 4.128906,4.1601562 0,2.28985 -1.839083,4.12935 -4.128906,4.128906 C 10.303927,17.315962 8.4648438,15.477907 8.4648438,13.1875 8.4648439,10.897093 10.30825,9.027344 12.59375,9.0273438 Z\" style=\"fill:#" +
                group.color +
                "\" />" +
                " <path d=\"m 12.594,1.3228953 c -6.573,0 -12.04399999,5.691 -12.04399999,11.8659997 0,2.778 1.56399999,6.308 2.69399999,8.746 l 9.306,17.872 9.262,-17.872 c 1.13,-2.438 2.738,-5.791 2.738,-8.746 0,-6.1749997 -5.383,-11.8659997 -11.956,-11.8659997 z m 0,7.155 c 2.584,0.017 4.679,2.1219997 4.679,4.7099997 0,2.588 -2.095,4.663 -4.679,4.679 -2.584,-0.017 -4.679,-2.09 -4.679,-4.679 0,-2.588 2.095,-4.6929997 4.679,-4.7099997 z\" style=\"fill:none;stroke:#000000;stroke-width:1.1;stroke-linecap:round;stroke-opacity:0.303\" />" +
                " <path d=\"m 12.55,37.429895 8.2655,-15.9845 c 1.1505,-2.4635 2.6405,-5.7215 2.6405,-8.2655 0,-5.5379997 -4.931,-10.7499997 -10.875,-10.7499997 -5.944,0 -10.938,5.219 -10.938,10.7499997 0,2.359 1.443,5.832 2.5785,8.2655 z M 12.581,7.3988953 c 3.168066,5e-4 5.781,2.6009999 5.781,5.7809997 0,3.18 -2.612937,5.781 -5.781,5.781 -3.1680631,0 -5.75,-2.6095 -5.75,-5.781 0,-3.1715 2.5819337,-5.7814997 5.75,-5.7809997 z\" style=\"fill:none;stroke:#ffffff;stroke-width:1.1;stroke-linecap:round;stroke-opacity:0.122\" />" +
                "  </svg>";
    }


    @GET
    @Path("/colors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getColors() {
        return em.createQuery("select distinct color from RegionGroup order by color", String.class)
                .getResultList();
    }

}
