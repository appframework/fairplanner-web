package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import de.christophbrill.fairplanner.persistence.selector.FairSelector;

import de.christophbrill.fairplanner.ui.dto.Fair;
import de.christophbrill.fairplanner.util.mappers.FairMapper;
import jakarta.ws.rs.Path;

@Path("/fair")
public class FairService extends AbstractResourceService<Fair, FairEntity> {

    @Override
    protected Class<FairEntity> getEntityClass() {
        return FairEntity.class;
    }

    @Override
    protected FairSelector getSelector() {
        return new FairSelector(em);
    }

    @Override
    protected FairMapper getMapper() {
        return FairMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return "ADMIN_FAIRS";
    }

    @Override
    protected String getReadPermission() {
        return "SHOW_FAIRS";
    }

    @Override
    protected String getUpdatePermission() {
        return "ADMIN_FAIRS";
    }

    @Override
    protected String getDeletePermission() {
        return "ADMIN_FAIRS";
    }

}
