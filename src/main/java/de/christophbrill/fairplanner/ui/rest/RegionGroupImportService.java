package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.appframework.ui.async.ProgressCache;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.NotAuthorizedException;
import de.christophbrill.fairplanner.persistence.model.ImportProcessEntity;
import de.christophbrill.fairplanner.persistence.model.Permission;
import de.christophbrill.fairplanner.ui.dto.Column;
import de.christophbrill.fairplanner.ui.dto.ImportConfiguration;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.fairplanner.ui.dto.RegionGroupImportColumns;
import de.christophbrill.fairplanner.util.imports.RegionGroupImportXlsx;
import de.christophbrill.fairplanner.util.imports.XlsxImportTools;
import io.quarkus.logging.Log;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.UserTransaction;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@Path("/regiongroupimport")
public class RegionGroupImportService {

    @Inject
    EntityManager em;
    @Inject
    UserTransaction transaction;
    @Inject
    ManagedExecutor executor;
    @Inject
    protected SecurityIdentity identity;

    @Inject
    RegionGroupImportXlsx regionGroupImportXlsx;
    @Inject
    ProgressCache progressCache;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public ImportConfiguration checkForUpload(@RestForm String filename,
                                              @RestForm("file") FileUpload file,
                                              @RestForm int sheet
    ) {
        if (!identity.hasRole(Permission.ADMIN_REGIONGROUPS.name())) {
            throw new NotAuthorizedException();
        }
        try {
            filename = filename.toLowerCase();

            List<Column> columns = new ArrayList<>();
            if (filename.endsWith(".xlsx") || filename.endsWith(".xls")) {
                XlsxImportTools.checkForUpload(Files.newInputStream(file.uploadedFile()), columns, sheet);
            } else {
                throw new BadArgumentException("Unsupported file ending");
            }

            return XlsxImportTools.storeImportConfiguration(em, filename, columns);
        } catch (WebApplicationException e) {
            throw e;
        } catch (IOException | RuntimeException e) {
            throw new BadArgumentException(e.getMessage());
        }
    }

    @POST
    @Path("import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String upload(@RestForm String filename,
                         @RestForm("file") FileUpload file,
                         @RestForm int identifier,
                         @RestForm @PartType(MediaType.APPLICATION_JSON) RegionGroupImportColumns columns,
                         @RestForm boolean removeExisting,
                         @RestForm int sheet
    ) {
        if (!identity.hasRole(Permission.ADMIN_REGIONGROUPS.name())) {
            throw new NotAuthorizedException();
        }

        Progress<ImportResult> progress = progressCache.createProgress();

        ImportProcessEntity importProcess = XlsxImportTools.startProcessImport(transaction, filename, columns, identifier, "NAME", progress);
        if (importProcess == null) {
            return progress.key;
        }

        byte[] data1;
        try {
            data1 = Files.readAllBytes(file.uploadedFile());
        } catch (IOException e) {
            throw new BadArgumentException(e.getMessage());
        }

        Uni.createFrom()
                .item(() -> progress.key)
                .invoke(uuid -> regionGroupImportXlsx.runImport(importProcess, removeExisting,
                        columns, data1, progress, sheet))
                .emitOn(executor)
                .subscribe()
                .with(done -> {
                    progress.completed = true;
                    Log.info("Completed import");
                }, e -> {
                    progress.completed = true;
                    progress.success = false;
                    progress.message = e.getMessage();
                    Log.error(e.getMessage(), e);
                });
        Log.infov("Import started {0}", progress.key);

        return progress.key;
    }

}
