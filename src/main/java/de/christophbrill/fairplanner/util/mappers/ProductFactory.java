package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.ProductEntity;
import de.christophbrill.fairplanner.ui.dto.Product;
import org.mapstruct.ObjectFactory;

public class ProductFactory {

    @ObjectFactory
    public ProductEntity createEntity(Product dto) {
        if (dto != null && dto.id != null) {
            ProductEntity entity = ProductEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Product with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new ProductEntity();
    }
    
}
