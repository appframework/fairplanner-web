package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.ui.dto.Customer;
import org.mapstruct.ObjectFactory;

public class CustomerFactory {

    @ObjectFactory
    public CustomerEntity createEntity(Customer dto) {
        if (dto != null && dto.id != null) {
            CustomerEntity entity = CustomerEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Customer with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new CustomerEntity();
    }
    
}
