package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity;
import de.christophbrill.fairplanner.ui.dto.ProductGroup;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(uses = ProductGroupFactory.class)
public interface ProductGroupMapper extends ResourceMapper<ProductGroup, ProductGroupEntity> {

    ProductGroupMapper INSTANCE = Mappers.getMapper(ProductGroupMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "products", ignore = true)
    ProductGroupEntity mapDtoToEntity(ProductGroup dto);

}
