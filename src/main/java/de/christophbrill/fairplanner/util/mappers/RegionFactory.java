package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.RegionEntity;
import de.christophbrill.fairplanner.ui.dto.Region;
import org.mapstruct.ObjectFactory;

public class RegionFactory {

    @ObjectFactory
    public RegionEntity createEntity(Region dto) {
        if (dto != null && dto.id != null) {
            RegionEntity entity = RegionEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Region with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new RegionEntity();
    }
    
}
