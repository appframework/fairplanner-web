package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import de.christophbrill.fairplanner.ui.dto.Fair;
import org.mapstruct.ObjectFactory;

public class FairFactory {

    @ObjectFactory
    public FairEntity createEntity(Fair dto) {
        if (dto != null && dto.id != null) {
            FairEntity entity = FairEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Fair with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new FairEntity();
    }
    
}
