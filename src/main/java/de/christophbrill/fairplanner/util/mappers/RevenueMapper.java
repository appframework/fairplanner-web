package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.RevenueEntity;
import de.christophbrill.fairplanner.ui.dto.Revenue;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = RevenueFactory.class)
public interface RevenueMapper extends ResourceMapper<Revenue, RevenueEntity> {

    RevenueMapper INSTANCE = Mappers.getMapper(RevenueMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "product", ignore = true)
    RevenueEntity mapDtoToEntity(Revenue dto);

}
