package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.persistence.model.QuotationEntity;
import de.christophbrill.fairplanner.ui.dto.Quotation;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {QuotationProductMapper.class, QuotationFactory.class})
public interface QuotationMapper extends ResourceMapper<Quotation, QuotationEntity> {

    QuotationMapper INSTANCE = Mappers.getMapper(QuotationMapper.class);

    @Override
    @Mapping(target = "customerId", ignore = true)
    Quotation mapEntityToDto(QuotationEntity entity);

    @AfterMapping
    default void afterEntityToDto(QuotationEntity entity, @MappingTarget Quotation dto) {
        dto.customerId = entity.customer.id;
    }

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "customer", ignore = true)
    QuotationEntity mapDtoToEntity(Quotation dto);

    default void afterDtoToEntity(Quotation dto, @MappingTarget QuotationEntity entity) {
        entity.customer = CustomerEntity.findById(dto.customerId);
    }

}
