package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.ui.dto.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CustomerFactory.class)
public interface CustomerMapper extends ResourceMapper<Customer, CustomerEntity> {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    CustomerEntity mapDtoToEntity(Customer dto);

}
