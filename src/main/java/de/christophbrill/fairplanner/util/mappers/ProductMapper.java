package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.ProductEntity;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity;
import de.christophbrill.fairplanner.ui.dto.Product;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(uses = {CostMapper.class, RevenueMapper.class, ProductFactory.class})
public interface ProductMapper extends ResourceMapper<Product, ProductEntity> {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    @Override
    @Mapping(target = "productGroupIds", ignore = true)
    Product mapEntityToDto(ProductEntity productEntity);

    @AfterMapping
    default void customEntityToDto(ProductEntity entity, @MappingTarget Product dto) {
        if (entity.productGroups != null) {
            dto.productGroupIds = entity.productGroups.stream().map(p -> p.id).collect(Collectors.toList());
        } else {
            dto.productGroupIds = Collections.emptyList();
        }
    }

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "productGroups", ignore = true)
    ProductEntity mapDtoToEntity(Product dto);

    @AfterMapping
    default void customDtoToEntity(Product dto, @MappingTarget ProductEntity entity) {
        if (CollectionUtils.isNotEmpty(dto.productGroupIds)) {
            List<ProductGroupEntity> projects = ProductGroupEntity.findByIds(dto.productGroupIds);
            if (entity.productGroups == null) {
                entity.productGroups = projects;
            } else {
                entity.productGroups.clear();
                entity.productGroups.addAll(projects);
            }
        } else {
            entity.productGroups = null;
        }
    }

}
