package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.ProductEntity;
import de.christophbrill.fairplanner.persistence.model.QuotationProductEntity;
import de.christophbrill.fairplanner.ui.dto.QuotationProduct;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = QuotationProduct.class)
public interface QuotationProductMapper extends ResourceMapper<QuotationProduct, QuotationProductEntity> {

    QuotationProductMapper INSTANCE = Mappers.getMapper(QuotationProductMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "product", ignore = true)
    @Mapping(target = "quotation", ignore = true)
    QuotationProductEntity mapDtoToEntity(QuotationProduct dto);

    default void afterDtoToEntity(QuotationProduct dto, @MappingTarget QuotationProductEntity entity) {
        entity.product = ProductEntity.findById(dto.productId);
    }

    @Override
    @Mapping(target = "productId", ignore = true)
    QuotationProduct mapEntityToDto(QuotationProductEntity entity);

    default void afterEntityToDto(QuotationProductEntity entity, @MappingTarget QuotationProduct dto) {
        dto.productId = entity.product.id;
    }

}
