package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import de.christophbrill.fairplanner.ui.dto.RegionGroup;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = RegionGroupFactory.class)
public interface RegionGroupMapper extends ResourceMapper<RegionGroup, RegionGroupEntity> {

    RegionGroupMapper INSTANCE = Mappers.getMapper(RegionGroupMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    RegionGroupEntity mapDtoToEntity(RegionGroup dto);

}
