package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.RegionEntity;
import de.christophbrill.fairplanner.ui.dto.LatLng;
import de.christophbrill.fairplanner.ui.dto.Region;
import org.locationtech.jts.geom.Coordinate;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

@Mapper(uses = RegionFactory.class)
public interface RegionMapper extends ResourceMapper<Region, RegionEntity> {

    RegionMapper INSTANCE = Mappers.getMapper(RegionMapper.class);

    @Override
    @Mapping(target = "geom", ignore = true)
    Region mapEntityToDto(RegionEntity entity);

    @AfterMapping
    default void customEntityToDto(RegionEntity entity, @MappingTarget Region dto) {
        if (entity.geom != null) {
            List<LatLng> coordinates = new ArrayList<>();
            for (Coordinate coordinate : entity.geom.getCoordinates()) {
                LatLng e = new LatLng();
                e.lat = coordinate.x;
                e.lng = coordinate.y;
                coordinates.add(e);
            }
            dto.geom = coordinates;
        } else {
            dto.geom = null;
        }
    }

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "geom", ignore = true)
    RegionEntity mapDtoToEntity(Region dto);

}
