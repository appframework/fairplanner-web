package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.OfferEntity;
import de.christophbrill.fairplanner.ui.dto.Offer;
import org.mapstruct.ObjectFactory;

public class OfferFactory {

    @ObjectFactory
    public OfferEntity createEntity(Offer dto) {
        if (dto != null && dto.id != null) {
            OfferEntity entity = OfferEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Offer with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new OfferEntity();
    }
    
}
