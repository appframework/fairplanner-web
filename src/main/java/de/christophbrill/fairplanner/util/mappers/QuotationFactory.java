package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.QuotationEntity;
import de.christophbrill.fairplanner.ui.dto.Quotation;
import org.mapstruct.ObjectFactory;

public class QuotationFactory {

    @ObjectFactory
    public QuotationEntity createEntity(Quotation dto) {
        if (dto != null && dto.id != null) {
            QuotationEntity entity = QuotationEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Quotation with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new QuotationEntity();
    }
    
}
