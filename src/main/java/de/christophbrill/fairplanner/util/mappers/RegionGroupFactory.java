package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import de.christophbrill.fairplanner.ui.dto.RegionGroup;
import org.mapstruct.ObjectFactory;

public class RegionGroupFactory {

    @ObjectFactory
    public RegionGroupEntity createEntity(RegionGroup dto) {
        if (dto != null && dto.id != null) {
            RegionGroupEntity entity = RegionGroupEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("RegionGroup with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new RegionGroupEntity();
    }
    
}
