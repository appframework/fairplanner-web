package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.RevenueEntity;
import de.christophbrill.fairplanner.ui.dto.Revenue;
import org.mapstruct.ObjectFactory;

public class RevenueFactory {

    @ObjectFactory
    public RevenueEntity createEntity(Revenue dto) {
        if (dto != null && dto.id != null) {
            RevenueEntity entity = RevenueEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Revenue with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new RevenueEntity();
    }
    
}
