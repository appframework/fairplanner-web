package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.CostEntity;
import de.christophbrill.fairplanner.ui.dto.Cost;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CostFactory.class)
public interface CostMapper extends ResourceMapper<Cost, CostEntity> {

    CostMapper INSTANCE = Mappers.getMapper(CostMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "product", ignore = true)
    CostEntity mapDtoToEntity(Cost dto);

}
