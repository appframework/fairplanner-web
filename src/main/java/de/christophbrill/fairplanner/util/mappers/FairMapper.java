package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import de.christophbrill.fairplanner.ui.dto.Fair;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = FairFactory.class)
public interface FairMapper extends ResourceMapper<Fair, FairEntity> {

    FairMapper INSTANCE = Mappers.getMapper(FairMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    FairEntity mapDtoToEntity(Fair dto);

}
