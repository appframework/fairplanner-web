package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import de.christophbrill.fairplanner.persistence.model.OfferEntity;
import de.christophbrill.fairplanner.ui.dto.Offer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ProductMapper.class, ProductGroupMapper.class, OfferFactory.class})
public interface OfferMapper extends ResourceMapper<Offer, OfferEntity> {

    OfferMapper INSTANCE = Mappers.getMapper(OfferMapper.class);

    @Override
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "fair", ignore = true)
    OfferEntity mapDtoToEntity(Offer dto);

    default void afterDtoToEntity(Offer dto, @MappingTarget OfferEntity entity) {
        entity.fair = FairEntity.findById(dto.fairId);
    }

    @Override
    @Mapping(target = "fairId", ignore = true)
    Offer mapEntityToDto(OfferEntity entity);

    default void afterEntityToDto(OfferEntity entity, @MappingTarget Offer dto) {
        dto.fairId = entity.fair.id;
    }
}
