package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.CostEntity;
import de.christophbrill.fairplanner.ui.dto.Cost;
import org.mapstruct.ObjectFactory;

public class CostFactory {

    @ObjectFactory
    public CostEntity createEntity(Cost dto) {
        if (dto != null && dto.id != null) {
            CostEntity entity = CostEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Cost with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new CostEntity();
    }
    
}
