package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.QuotationProductEntity;
import de.christophbrill.fairplanner.ui.dto.QuotationProduct;
import org.mapstruct.ObjectFactory;

public class QuotationProductFactory {

    @ObjectFactory
    public QuotationProductEntity createEntity(QuotationProduct dto) {
        if (dto != null && dto.id != null) {
            QuotationProductEntity entity = QuotationProductEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("QuotationProduct with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new QuotationProductEntity();
    }
    
}
