package de.christophbrill.fairplanner.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity;
import de.christophbrill.fairplanner.ui.dto.ProductGroup;
import org.mapstruct.ObjectFactory;

public class ProductGroupFactory {

    @ObjectFactory
    public ProductGroupEntity createEntity(ProductGroup dto) {
        if (dto != null && dto.id != null) {
            ProductGroupEntity entity = ProductGroupEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("ProductGroup with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new ProductGroupEntity();
    }
    
}
