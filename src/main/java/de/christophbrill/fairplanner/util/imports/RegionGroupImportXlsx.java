package de.christophbrill.fairplanner.util.imports;

import static de.christophbrill.fairplanner.util.imports.XlsxImportTools.getFirstRowIndexHavingMaxWidth;
import static de.christophbrill.fairplanner.util.imports.XlsxImportTools.getValue;

import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.ImportProcessEntity;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import de.christophbrill.fairplanner.persistence.model.RegionGroupZipEntity;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.fairplanner.ui.dto.RegionGroupImportColumns;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.UserTransaction;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

@ApplicationScoped
public class RegionGroupImportXlsx {

    @Inject
    EntityManager entityManager;
    @Inject
    UserTransaction transaction;

    RegionGroupEntity getRegionGroupEntity(@Nonnull String name) {
        if (name.isEmpty()) {
            return null;
        }
        var regionGroup = RegionGroupEntity.<RegionGroupEntity>find("name", name)
                .firstResult();
        if (regionGroup == null) {
            regionGroup = new RegionGroupEntity();
            regionGroup.name = name;
            Random random = new Random();
            regionGroup.color = randomNumber(random) + randomNumber(random) + randomNumber(random);
        }
        return regionGroup;
    }

    private String randomNumber(Random random) {
        return String.format("%02X", random.nextInt(255));
    }

    public void upload(Progress<ImportResult> progress, ImportResult result, byte[] data, int nameColumn, RegionGroupImportColumns columns, int sheet_)
            throws HeuristicRollbackException, SystemException, HeuristicMixedException, RollbackException, NotSupportedException, IOException {
        try (ByteArrayInputStream stream = new ByteArrayInputStream(data);
             XSSFWorkbook wb = new XSSFWorkbook(stream)) {
            XSSFSheet sheet = wb.getSheetAt(sheet_ - 1);

            progress.max = sheet.getLastRowNum();
            progress.message = ("Importing regionGroups ...");
            transaction.begin();

            for (int i = getFirstRowIndexHavingMaxWidth(sheet) + 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                progress.value = i;

                if (i % 100 == 0) {
                    entityManager.flush();
                    transaction.commit();
                    entityManager.clear();
                    transaction.begin();
                    Log.debugv("Imported {0} regionGroups", i);
                    progress.message = "Importing regionGroup " + i;
                }

                // No row, cannot continue
                if (row == null) {
                    result.skipped++;
                    continue;
                }

                Cell cell = row.getCell(nameColumn);
                if (cell == null) {
                    result.skipped++;
                    continue;
                }

                RegionGroupEntity regionGroup;
                CellType type = cell.getCellType();
                if (type == CellType.STRING) {
                    String value = cell.getStringCellValue();
                    regionGroup = getRegionGroupEntity(value);
                    if (regionGroup == null) {
                        result.skipped++;
                        continue;
                    }
                } else if (type == CellType.NUMERIC) {
                    String value = Integer.toString((int) cell.getNumericCellValue());
                    regionGroup = getRegionGroupEntity(value);
                    if (regionGroup == null) {
                        result.skipped++;
                        continue;
                    }
                } else if (type == CellType.BLANK) {
                    result.skipped++;
                    continue;
                } else {
                    throw new BadArgumentException("Not handled " + type);
                }

                if (regionGroup.zips == null) {
                    regionGroup.zips = new ArrayList<>();
                }

                RegionGroupZipEntity e = new RegionGroupZipEntity();
                e.from = getValue(row, columns.from.getFirst(), "from");
                e.to = getValue(row, columns.to.getFirst(), "to");
                regionGroup.zips.add(e);
                result.created++;

                regionGroup.persist();

            }
            transaction.commit();
            progress.message = "Import completed";
            progress.completed = true;
            progress.success = true;
        } catch (IOException | RuntimeException e) {
            progress.message = e.getMessage();
            progress.completed = true;
            progress.success = false;
            transaction.rollback();
            throw e;
        }
    }



    public void runImport(ImportProcessEntity importProcess, boolean removeExisting, RegionGroupImportColumns columns,
                          byte[] data, Progress<ImportResult> progress, int sheet) {
        Log.debug("Consuming import event");

        ImportResult result = new ImportResult();
        try {
            if (removeExisting) {
                transaction.begin();
                result.deleted = entityManager.createQuery("delete from RegionGroup").executeUpdate();
                transaction.commit();
            }

            if (importProcess.filename.endsWith(".xlsx") ||
                    importProcess.filename.endsWith(".xls")) {
                upload(progress, result, data, importProcess.identifierColumn,
                        columns, sheet);
            } else {
                throw new BadArgumentException("Unsupported file ending");
            }
        } catch (Exception e) {
            progress.success = false;
            progress.message = e.getMessage();
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            progress.result = result;
            progress.completed = true;
        }
    }

}
