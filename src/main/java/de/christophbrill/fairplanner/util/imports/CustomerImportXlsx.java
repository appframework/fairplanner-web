package de.christophbrill.fairplanner.util.imports;

import static de.christophbrill.fairplanner.util.imports.XlsxImportTools.getFirstRowIndexHavingMaxWidth;
import static de.christophbrill.fairplanner.util.imports.XlsxImportTools.getValue;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.AddressEntity;
import de.christophbrill.fairplanner.persistence.model.ContactEntity;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.persistence.model.IdentifierEntity;
import de.christophbrill.fairplanner.ui.dto.Contact;
import de.christophbrill.fairplanner.ui.dto.Contact.Type;
import de.christophbrill.fairplanner.ui.dto.CustomerImportColumns;
import de.christophbrill.appframework.ui.dto.ImportResult;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.UserTransaction;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class CustomerImportXlsx {

    @Inject
    EntityManager entityManager;
    @Inject
    UserTransaction transaction;

    @Inject
    CustomerImportUtil customerImportUtil;

    public void upload(Progress<ImportResult> progress, ImportResult result, byte[] data, CustomerImportColumns columns,
                       int identifierColumn, String identifierType, boolean skipGeocoding, int sheet_) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException, IOException {

        List<Integer> nameColumns = columns.name;
        List<Integer> streetColumns = columns.street;
        List<Integer> zipColumns = columns.zip;
        List<Integer> cityColumns = columns.city;
        Map<Contact.Type, List<Integer>> contactColumns = columns.contact;
        Map<String, List<Integer>> identifierColumns = columns.identifiers;

        try (ByteArrayInputStream stream = new ByteArrayInputStream(data);
             XSSFWorkbook wb = new XSSFWorkbook(stream)) {
            XSSFSheet sheet = wb.getSheetAt(sheet_ - 1);

            ObjectMapper mapper = new ObjectMapper();

            progress.max = sheet.getLastRowNum();
            progress.message = ("Importing customers ...");
            transaction.begin();

            for (int i = getFirstRowIndexHavingMaxWidth(sheet) + 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                progress.value = i;

                if (i % 10 == 0) {
                    entityManager.flush();
                    transaction.commit();
                    entityManager.clear();
                    transaction.begin();
                    Log.debugv("Imported {0} customers", i);
                }
                progress.message = "Importing customer " + i;

                // No row, cannot continue
                if (row == null) {
                    continue;
                }

                Cell cell = row.getCell(identifierColumn);
                if (cell == null) {
                    continue;
                }

                CustomerEntity customer;
                CellType type = cell.getCellType();
                if (type == CellType.STRING) {
                    String value = cell.getStringCellValue();
                    customer = customerImportUtil.getCustomerEntity(identifierType, value);
                    if (customer == null) {
                        result.skipped++;
                        continue;
                    }
                } else if (type == CellType.NUMERIC) {
                    String value = Integer.toString((int) cell.getNumericCellValue());
                    customer = customerImportUtil.getCustomerEntity(identifierType, value);
                    if (customer == null) {
                        result.skipped++;
                        continue;
                    }
                } else if (type == CellType.BLANK) {
                    continue;
                } else {
                    throw new BadArgumentException("Not handled " + type);
                }

                if (customer.id != null) {
                    result.updated++;
                } else {
                    result.created++;
                }

                customer.name = getValue(row, nameColumns, "name");

                AddressEntity address = customer.address;
                if (address == null) {
                    address = new AddressEntity();
                    customer.address = address;
                }
                address.street = getValue(row, streetColumns, "street");
                address.city = getValue(row, cityColumns, "city");
                address.zip = getValue(row, zipColumns, "zip");

                if (contactColumns != null) {
                    if (customer.contactdata == null) {
                        customer.contactdata = new HashSet<>();
                    } else {
                        customer.contactdata.clear();
                    }
                    for (Map.Entry<Type, List<Integer>> entry : contactColumns.entrySet()) {
                        String contactdata = getValue(row, entry.getValue(), entry.getKey().name());
                        if (StringUtils.isNotEmpty(contactdata)) {
                            customer.contactdata.add(new ContactEntity(entry.getKey(), contactdata));
                        }
                    }
                }

                if (identifierColumns != null) {
                    outer:
                    for (Map.Entry<String, List<Integer>> entry : identifierColumns.entrySet()) {
                        String additionalIdentifierType = entry.getKey();
                        for (IdentifierEntity additionalIdentifier : customer.identifiers) {
                            if (additionalIdentifier.type.equals(additionalIdentifierType)) {
                                additionalIdentifier.value = getValue(row, entry.getValue(), entry.getKey());
                                continue outer;
                            }
                        }
                        customer.identifiers.add(new IdentifierEntity(entry.getKey(), getValue(row, entry.getValue(), entry.getKey())));
                    }
                }

                customerImportUtil.storeWithCoordinate(mapper, customer, address, skipGeocoding);
            }
            transaction.commit();
            progress.message = "Import completed";
            progress.completed = true;
            progress.success = true;
        } catch (IOException | RuntimeException e) {
            progress.message = e.getMessage();
            progress.completed = true;
            progress.success = false;
            transaction.rollback();
            throw e;
        }
    }

}
