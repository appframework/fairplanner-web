package de.christophbrill.fairplanner.util.imports;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.fairplanner.persistence.model.AddressEntity;
import de.christophbrill.fairplanner.persistence.model.ContactEntity;
import de.christophbrill.fairplanner.persistence.model.IdentifierEntity;
import de.christophbrill.fairplanner.ui.dto.Column;
import de.christophbrill.fairplanner.ui.dto.Contact;
import de.christophbrill.fairplanner.ui.dto.Contact.Type;
import de.christophbrill.fairplanner.ui.dto.CustomerImportColumns;
import de.christophbrill.appframework.ui.dto.ImportResult;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.UserTransaction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.mozilla.universalchardet.UniversalDetector;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class CustomerImportCsv {

    @Inject
    EntityManager entityManager;
    @Inject
    UserTransaction transaction;

    @Inject
    CustomerImportUtil customerImportUtil;

    public static void checkForUpload(InputStream file, List<Column> result) throws IOException {
        try (Reader reader = new InputStreamReader(file);
             CSVParser parser = CSVFormat.DEFAULT.builder()
                     .setDelimiter(';')
                     .setHeader().setSkipHeaderRecord(true)
                     .build()
                     .parse(reader)) {
            for (Map.Entry<String, Integer> entry : parser.getHeaderMap().entrySet()) {
                result.add(new Column(entry.getValue(), entry.getKey()));
            }
        }
    }

    public void upload(Progress<ImportResult> progress, ImportResult result, byte[] data, CustomerImportColumns columns,
                       int identifierColumn, String identifierType, boolean skipGeocoding) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException, IOException {

        List<Integer> nameColumns = columns.name;
        List<Integer> streetColumns = columns.street;
        List<Integer> zipColumns = columns.zip;
        List<Integer> cityColumns = columns.city;
        Map<Contact.Type, List<Integer>> contactColumns = columns.contact;
        Map<String, List<Integer>> identifierColumns = columns.identifiers;

        try (ByteArrayInputStream stream = new ByteArrayInputStream(data)) {
            stream.mark(10 * 4096);

            String encoding = null;
            try {
                UniversalDetector detector = new UniversalDetector();
                int nread;
                byte[] buf = new byte[4096];
                while ((nread = stream.read(buf)) > 0 && !detector.isDone()) {
                    detector.handleData(buf, 0, nread);
                }
                detector.dataEnd();
                encoding = detector.getDetectedCharset();
                detector.reset();

                stream.reset();
            } catch (IOException e) {
                Log.infov("Failed to detect the charset: {0}", e.getMessage());
            }

            if (encoding == null) {
                encoding = "UTF-8";
            }

            try (InputStreamReader reader = new InputStreamReader(stream, encoding);
                 CSVParser parser = CSVFormat.DEFAULT.builder()
                         .setDelimiter(';').setHeader().setSkipHeaderRecord(true)
                         .build()
                         .parse(reader)) {

                ObjectMapper mapper = new ObjectMapper();
                int count = 0;

                progress.message = ("Importing customers ...");
                transaction.begin();

                for (CSVRecord row : parser) {
                    count++;
                    progress.value = count;
                    if (count % 100 == 0) {
                        entityManager.flush();
                        transaction.commit();
                        entityManager.clear();
                        transaction.begin();
                        Log.debugv("Imported {0} customers", count);
                        progress.message = ("Importing customer " + count);
                    }

                    // No row, cannot continue
                    if (row == null) {
                        continue;
                    }

                    String value = row.get(identifierColumn);
                    if (value == null) {
                        continue;
                    }

                    var customer = customerImportUtil.getCustomerEntity(identifierType, value);
                    if (customer != null) {
                        if (customer.id != null) {
                            result.updated++;
                        } else {
                            result.created++;
                        }
                        customer.name = getValue(row, nameColumns);

                        AddressEntity address = customer.address;
                        if (address == null) {
                            address = new AddressEntity();
                            customer.address = address;
                        }
                        address.street = getValue(row, streetColumns);
                        address.city = getValue(row, cityColumns);
                        address.zip = getValue(row, zipColumns);

                        if (contactColumns != null) {
                            if (customer.contactdata == null) {
                                customer.contactdata = new HashSet<>();
                            } else {
                                customer.contactdata.clear();
                            }
                            for (Map.Entry<Type, List<Integer>> entry : contactColumns.entrySet()) {
                                String contactdata = getValue(row, entry.getValue());
                                if (StringUtils.isNotEmpty(contactdata)) {
                                    customer.contactdata.add(new ContactEntity(entry.getKey(), contactdata));
                                }
                            }
                        }

                        if (identifierColumns != null) {
                            outer:
                            for (Map.Entry<String, List<Integer>> entry : identifierColumns.entrySet()) {
                                String additionalIdentifierType = entry.getKey();
                                for (IdentifierEntity additionalIdentifier : customer.identifiers) {
                                    if (additionalIdentifier.type.equals(additionalIdentifierType)) {
                                        additionalIdentifier.value = getValue(row, entry.getValue());
                                        continue outer;
                                    }
                                }
                                customer.identifiers.add(new IdentifierEntity(entry.getKey(), getValue(row, entry.getValue())));
                            }
                        }

                        customerImportUtil.storeWithCoordinate(mapper, customer, address, skipGeocoding);
                    } else {
                        result.skipped++;
                    }
                }
            }
            transaction.commit();
            progress.message = "Import completed";
            progress.completed = true;
            progress.success = true;
        } catch (IOException | RuntimeException e) {
            Log.error(e.getMessage(), e);
            progress.message = e.getMessage();
            progress.completed = true;
            progress.success = false;
            transaction.rollback();
            throw e;
        }
    }

    private String getValue(@Nonnull CSVRecord row, @Nullable List<Integer> columns) {
        if (CollectionUtils.isEmpty(columns)) {
            return "";
        }
        StringBuilder b = new StringBuilder();
        for (Integer i : columns) {
            String s = row.get(i);
            if (StringUtils.isNotEmpty(s)) {
                if (!b.isEmpty()) {
                    b.append(' ');
                }
                b.append(s);
            }
        }
        return b.toString();
    }
}
