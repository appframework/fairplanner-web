package de.christophbrill.fairplanner.util.imports;

import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.RegionEntity;
import de.christophbrill.appframework.ui.dto.ImportResult;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.UserTransaction;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.PrecisionModel;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class RegionImport {

    @Inject
    EntityManager em;
    @Inject
    UserTransaction transaction;
    @Inject
    RegionImportOsmPbf regionImportOsmPbf;
    @Inject
    RegionImportShp regionImportShp;

        public void runImport(String filename, boolean removeExisting, FileUpload file, String include,
                          Progress<ImportResult> progress) {
        ImportResult result = new ImportResult();

        Map<String, List<Coordinate>> regions;
        if (filename.endsWith(".osm.pbf")) {
            regions = regionImportOsmPbf.upload(file, include, progress, result);
        } else if (filename.endsWith(".shp.zip")) {
            regions = regionImportShp.uploadZip(file, include, progress);
        } else if (filename.endsWith(".shp")) {
            regions = regionImportShp.upload(file, include, progress, filename);
        } else {
            progress.completed = true;
            progress.message = "Invalid file format";
            throw new BadArgumentException("Invalid file format");
        }

        try {
            if (removeExisting) {
                transaction.begin();
                result.deleted = em.createQuery("delete from Region").executeUpdate();
                transaction.commit();
            }
            int current = 0;
            try {
                progress.max = regions.entrySet().size();
                progress.message = ("Importing regions ...");
                transaction.begin();
                for (Map.Entry<String, List<Coordinate>> region : regions.entrySet()) {
                    progress.value = current++;
                    progress.message = "Importing region " + region.getKey();
                    result.created++;
                    List<Coordinate> value = region.getValue();
                    RegionEntity entity = new RegionEntity();
                    entity.zip = region.getKey();
                    GeometryFactory factory = new GeometryFactory(new PrecisionModel(PrecisionModel.FIXED), 4326);
                    // Close ring
                    if (!value.getFirst().equals2D(value.getLast())) {
                        value.add(value.getFirst());
                    }
                    try {
                        LinearRing shell = factory.createLinearRing(value.toArray(new Coordinate[0]));
                        entity.geom = new Polygon(shell, null, factory);
                        entity.persist();
                    } catch (IllegalArgumentException e) {
                        Log.warn(e.getMessage(), e);
                    }
                }
                transaction.commit();
                progress.message = "Import completed";
                progress.completed = true;
                progress.success = true;
                progress.result = result;
            } catch (RuntimeException e) {
                progress.message = e.getMessage();
                progress.completed = true;
                progress.success = false;
                transaction.rollback();
                throw e;
            }
        } catch (Exception e) {
            progress.success = false;
            progress.message = e.getMessage();
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            progress.completed = true;
        }
    }

}
