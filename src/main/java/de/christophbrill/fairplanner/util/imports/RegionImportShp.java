package de.christophbrill.fairplanner.util.imports;

import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.dto.ImportResult;
import io.quarkus.logging.Log;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import org.apache.commons.lang3.StringUtils;
import org.geotools.api.data.DataStore;
import org.geotools.api.data.DataStoreFinder;
import org.geotools.api.data.FeatureSource;
import org.geotools.api.feature.simple.SimpleFeature;
import org.geotools.api.feature.simple.SimpleFeatureType;
import org.geotools.api.filter.Filter;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.MultiPolygon;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@ApplicationScoped
public class RegionImportShp {

    private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    public Map<String, List<Coordinate>> uploadZip(FileUpload file, String include, Progress<ImportResult> progress) {
        var fileZip = file.uploadedFile();
        var destDir = file.uploadedFile().getParent().resolve("unzipped");

        try {
            Files.createDirectories(destDir);

            byte[] buffer = new byte[1024];
            ZipInputStream zis = new ZipInputStream(Files.newInputStream(fileZip));
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                File newFile = newFile(destDir.toFile(), zipEntry);
                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                } else {
                    // fix for Windows-created archives
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }

                    // write file content
                    FileOutputStream fos = new FileOutputStream(newFile);
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                }
                zipEntry = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            try {
                return importShp(destDir.resolve("plz-5stellig.shp"), include);
            } catch (IOException | RuntimeException e) {
                progress.completed = true;
                progress.message = e.getMessage();
                throw new RuntimeException("Failed to read file in .shp format", e);
            }

        } catch (IOException e) {
            progress.completed = true;
            progress.message = e.getMessage();
            throw new RuntimeException("Failed to read file in .shp.zip format", e);
        }
    }

    private Map<String, List<Coordinate>> importShp(Path path, @Nullable String include) throws IOException {
        Map<String, Object> map = new HashMap<>();
        map.put("url", path.toUri().toURL());

        DataStore dataStore = DataStoreFinder.getDataStore(map);
        if (dataStore == null) {
            throw new BadArgumentException("Datastore is null for path: " + path);
        }
        String typeName = dataStore.getTypeNames()[0];

        FeatureSource<SimpleFeatureType, SimpleFeature> source =
                dataStore.getFeatureSource(typeName);
        Filter filter = Filter.INCLUDE;

        FeatureCollection<SimpleFeatureType, SimpleFeature> collection = source.getFeatures(filter);
        Map<String, List<Coordinate>> result = new HashMap<>();
        try (FeatureIterator<SimpleFeature> features = collection.features()) {
            while (features.hasNext()) {
                SimpleFeature feature = features.next();
                if (feature.getDefaultGeometryProperty().getValue() instanceof MultiPolygon poly) {
                    String postalCodeValue = feature.getAttribute("plz").toString();
                    if (StringUtils.isEmpty(include) || include.equals(postalCodeValue)) {
                        List<Coordinate> list = new ArrayList<>();
                        for (var ads : poly.getCoordinates()) {
                            // Latitude and longitude are mixed up in the shapefile
                            list.add(new Coordinate(ads.y, ads.x));
                        }
                        result.put(postalCodeValue, list);
                    }
                } else {
                    Log.errorv("Unknown type {0}", feature.getDefaultGeometryProperty().getValue().getClass());
                }
            }
        }

        return result;
    }

    public Map<String, List<Coordinate>> upload(FileUpload file, String include, Progress<ImportResult> progress, String filename) {
        var target = file.uploadedFile().getParent().resolve(filename);
        try {
            Files.deleteIfExists(target);
        } catch (IOException e) {
            Log.error(e.getMessage(), e);
            throw new BadArgumentException("Failed to read file in .shp format");
        }
        try {
            Files.move(file.uploadedFile(), target);
            return importShp(target, include);
        } catch (IOException | RuntimeException e) {
            progress.completed = true;
            progress.message = e.getMessage();
            throw new RuntimeException("Failed to read file in .shp format", e);
        }
    }
}
