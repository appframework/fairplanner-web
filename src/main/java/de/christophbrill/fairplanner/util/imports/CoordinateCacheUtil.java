package de.christophbrill.fairplanner.util.imports;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import de.christophbrill.fairplanner.persistence.model.CoordinateCacheEntity;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class CoordinateCacheUtil {

    @Nonnull
    public static CoordinateCacheEntity evaluateCoordinate(@Nonnull ObjectMapper mapper, @Nonnull String nominatimBaseUri,
                                                           @Nullable String street, @Nullable String postalCode, @Nullable String city,
                                                           String version) {
        String uri = buildUrl(street, postalCode, city);

        var lookupCache = CoordinateCacheEntity.<CoordinateCacheEntity>find("query", uri)
                .firstResult();

        if (lookupCache == null) {
            lookupCache = new CoordinateCacheEntity();
            lookupCache.query = uri;
            Log.debugv("Need to download {0}", uri);
            JsonNode jsonNode;
            try {
                URLConnection urlConnection = new URI(nominatimBaseUri + uri).toURL().openConnection();
                urlConnection.setRequestProperty("User-Agent", "FairPlanner/" + version + " (https://gitlab.com/appframework/fairplanner-web)");

                jsonNode = mapper.readTree(urlConnection.getInputStream());
            } catch (IOException | URISyntaxException e) {
                Log.errorv("Could not download coordinates from {0}", uri, e);
                return lookupCache;
            }
            assert jsonNode instanceof ArrayNode;

            ArrayNode array = (ArrayNode) jsonNode;
            if (array.isEmpty()) {
                Log.debugv("No response for {0}{1}", nominatimBaseUri, uri);
            }
            for (JsonNode node : array) {
                lookupCache.lat = Double.valueOf(node.get("lat").textValue());
                lookupCache.lon = Double.valueOf(node.get("lon").textValue());
            }
            lookupCache.persist();
        }
        return lookupCache;
    }

    @Nonnull
    private static String buildUrl(@Nullable String street, @Nullable String zip, @Nullable String city) {
        String uri = "format=json&street={street}&city={city}&postalcode={postalcode}&country=Germany&limit=1";
        uri = uri.replace("{street}", street != null ? URLEncoder.encode(street, StandardCharsets.UTF_8) : "");
        uri = uri.replace("{postalcode}", zip != null ? URLEncoder.encode(zip, StandardCharsets.UTF_8) : "");
        uri = uri.replace("{city}", city != null ? URLEncoder.encode(city, StandardCharsets.UTF_8) : "");
        return uri;
    }

}
