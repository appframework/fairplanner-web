package de.christophbrill.fairplanner.util.imports;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.ImportProcessEntity;
import de.christophbrill.fairplanner.persistence.selector.ImportProcessSelector;
import de.christophbrill.fairplanner.ui.dto.Column;
import de.christophbrill.fairplanner.ui.dto.ImportConfiguration;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.persistence.EntityManager;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.UserTransaction;
import jakarta.ws.rs.WebApplicationException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class XlsxImportTools {

    private static class ValueComparator<K, V extends Comparable<V>> implements Comparator<K> {
        private final Map<K, V> map;

        public ValueComparator(Map<K, V> base) {
            this.map = base;
        }

        @Override
        public int compare(K o1, K o2) {
            return map.get(o2).compareTo(map.get(o1));
        }
    }

    public static ImportConfiguration analyzeUpload(EntityManager em,
                                                    FileUpload file,
                                                    String filename,
                                                    Integer sheet) {
        try {
            filename = filename.toLowerCase();

            List<Column> columns = new ArrayList<>();
            XlsxImportTools.checkForUpload(Files.newInputStream(file.uploadedFile()), columns, sheet);

            return storeImportConfiguration(em, filename, columns);
        } catch (WebApplicationException e) {
            throw e;
        } catch (IOException | RuntimeException e) {
            throw new BadArgumentException(e.getMessage());
        }
    }

    public static ImportConfiguration storeImportConfiguration(EntityManager em, String filename, List<Column> columns) {
        ImportProcessEntity importprocess = new ImportProcessSelector(em)
                .withFilename(filename)
                .withSortColumn("created", false)
                .withLimit(1)
                .find();

        ImportConfiguration result = new ImportConfiguration();
        result.columns = columns;
        if (importprocess != null) {
            result.configuration = importprocess.columns;
            result.identifierColumn = importprocess.identifierColumn;
            result.identifierType = importprocess.identifierType;
            result.sheet = importprocess.sheet;
        }
        return result;
    }

    private static Row getFirstRowHavingMaxWidth(XSSFSheet sheet) {
        int mostused = getMostUsedWidth(sheet);

        for (Row row : sheet) {
            if (row == null) {
                continue;
            }
            if (row.getLastCellNum() >= mostused) {
                return row;
            }
        }
        return null;
    }

    private static int getMostUsedWidth(XSSFSheet sheet) {
        Map<Short, Integer> widths = new HashMap<>();
        for (Row row : sheet) {
            Short lastCellNum = row.getLastCellNum();
            widths.merge(lastCellNum, 1, Integer::sum);
        }

        ValueComparator<Short, Integer> comparator = new ValueComparator<>(widths);
        Map<Short, Integer> sortedMap = new TreeMap<>(comparator);
        sortedMap.putAll(widths);
        List<Short> sortedList = new ArrayList<>(sortedMap.keySet());
        return sortedList.getFirst();
    }

    public static int getFirstRowIndexHavingMaxWidth(XSSFSheet sheet) {
        int mostused = getMostUsedWidth(sheet);

        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            if (row.getLastCellNum() >= mostused) {
                return i;
            }
        }
        return -1;
    }

    protected static int getSheetNumber(Integer sheet) {
        if (sheet == null) {
            sheet = 1;
        }
        return sheet - 1;
    }

    public static void checkForUpload(InputStream stream, List<Column> result, Integer givenSheetNumber) throws IOException {
        try (XSSFWorkbook wb = new XSSFWorkbook(stream)) {
            XSSFSheet sheet = wb.getSheetAt(getSheetNumber(givenSheetNumber));
            Row row = getFirstRowHavingMaxWidth(sheet);
            if (row == null) {
                throw new BadArgumentException("No content found, make sure to have the first and second column filled");
            }
            for (int i = 0; i < row.getLastCellNum(); i++) {
                String value = getValue(row, i, null);
                if (value == null) {
                    continue;
                }
                if (!value.isEmpty()) {
                    result.add(new Column(i, value));
                }
            }
        }
    }

    /**
     * Get the value of multiple columns and concatenate them with whitespace.
     *
     * @param row        the row to work on
     * @param columns    the numbers of the columns (0 based)
     * @param columnName (optional) column name used in log messages
     * @return the value of the columns
     */
    @Nonnull
    public static String getValue(@Nonnull Row row, @Nonnull List<Integer> columns, @Nullable String columnName) {
        if (CollectionUtils.isEmpty(columns)) {
            return "";
        }
        StringBuilder b = new StringBuilder();
        for (Integer j : columns) {
            String s = getValue(row, j, columnName);
            if (StringUtils.isNotEmpty(s)) {
                if (!b.isEmpty()) {
                    b.append(' ');
                }
                b.append(s);
            }
        }
        return b.toString();
    }

    /**
     * Get the value from a given column from an XLSX sheet.
     *
     * @param row        the row to work on
     * @param column     the number of the column (0 based)
     * @param columnName (optional) column name used in log messages
     * @return the value of the column
     */
    @Nullable
    public static String getValue(@Nonnull Row row, @Nullable Integer column, @Nullable String columnName) {
        // If the caller did not provide a column number, we cannot find anything here
        if (column == null) {
            return null;
        }

        // Load the cell in row and column
        Cell cell = row.getCell(column);
        if (cell == null) {
            return null;
        }

        // Get the content from the cell and convert it to a string
        CellType columnType = cell.getCellType();
        switch (columnType) {
            case CellType.STRING, CellType.FORMULA, CellType.BLANK -> {
                return cell.getStringCellValue();
            }
            case CellType.NUMERIC -> {
                return String.format("%05d", (int) cell.getNumericCellValue());
            }
            default -> {
                Log.warnv("Skipping {0} {1}", columnName != null ? columnName : column, columnType);
                return null;
            }
        }
    }

    public static <C> ImportProcessEntity startProcessImport(UserTransaction transaction, String filename, C columns,
                                                             int identifier, String identifierType, Progress<?> progress) {
        try {
            transaction.begin();
            ImportProcessEntity importProcess = new ImportProcessEntity();
            importProcess.filename = filename;
            importProcess.columns = new ObjectMapper().writeValueAsString(columns);
            importProcess.identifierType = identifierType;
            importProcess.identifierColumn = identifier;
            importProcess.persist();
            transaction.commit();
            return importProcess;
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SystemException |
                 NotSupportedException | JsonProcessingException e) {
            progress.completed = true;
            progress.success = false;
            progress.message = e.getMessage();
            return null;
        }
    }

}
