package de.christophbrill.fairplanner.util.imports;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.fairplanner.persistence.model.AddressEntity;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.persistence.model.IdentifierEntity;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class CustomerImportUtil {

    @ConfigProperty(name = "quarkus.application.version")
    String version;

    CustomerEntity getCustomerEntity(String identifierType, @Nonnull String value) {
        if (value.isEmpty()) {
            return null;
        }
        var customer = CustomerEntity.findByIdentifier(identifierType, value);
        if (customer == null) {
            customer = new CustomerEntity();
            customer.identifiers.add(new IdentifierEntity(identifierType, value));
        }
        return customer;
    }

    protected void storeWithCoordinate(ObjectMapper mapper, CustomerEntity customer,
                                                 AddressEntity address, boolean skipGeocoding) {
        if (!skipGeocoding) {
            var cache = CoordinateCacheUtil.evaluateCoordinate(mapper,
                    "https://nominatim.openstreetmap.org/search?", address.street,
                    address.zip, address.city, version);
            // Fall back to search without city name, which can differ to much
            if (cache.lat == null && cache.lon == null) {
                cache = CoordinateCacheUtil.evaluateCoordinate(mapper,
                        "https://nominatim.openstreetmap.org/search?",
                        address.street, address.zip,
                        null, version);
            }
            if (cache.lat == null && cache.lon == null) {
                Log.warnv("Failed to geocode {0}, {1}, {2}", address.street, address.zip, address.city);
            }
            customer.address.lat = cache.lat;
            customer.address.lon = cache.lon;
        }
        customer.persist();
    }
}
