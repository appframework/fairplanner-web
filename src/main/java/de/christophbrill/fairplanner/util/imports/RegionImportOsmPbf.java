package de.christophbrill.fairplanner.util.imports;

import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.dto.ImportResult;
import io.quarkus.logging.Log;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.locationtech.jts.geom.Coordinate;
import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.Bound;
import org.openstreetmap.osmosis.core.domain.v0_6.Entity;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;
import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
import org.openstreetmap.osmosis.core.domain.v0_6.RelationMember;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.domain.v0_6.WayNode;
import org.openstreetmap.osmosis.core.task.v0_6.Sink;
import org.openstreetmap.osmosis.pbf2.v0_6.PbfReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class RegionImportOsmPbf {

    public Map<String, List<Coordinate>> upload(FileUpload file, String include, Progress<ImportResult> progress, ImportResult result) {
        try (InputStream stream = Files.newInputStream(file.uploadedFile())) {
            return importPbf(stream, include, progress, result);
        } catch (IOException | RuntimeException e) {
            progress.completed = true;
            progress.message = e.getMessage();
            throw new RuntimeException("Failed to read file in .osm.pbf format", e);
        }
    }

    Map<String, List<Coordinate>> importPbf(InputStream stream, @Nullable String include,
                                                   Progress<ImportResult> progress, ImportResult result_) {

        PbfReader reader = new PbfReader(() -> stream, 1);

        Map<Long, Node> nodes = new HashMap<>();
        Map<Long, Way> ways = new HashMap<>();

        Map<String, List<Coordinate>> result = new HashMap<>();

        Sink sinkImplementation = new Sink() {

            @Override
            public void process(EntityContainer entityContainer) {
                progress.value++;

                Entity entity = entityContainer.getEntity();
                switch (entity) {
                    case Node node ->
                        // We are going to read the nodes first, just store them into a temporary cache
                            nodes.put(node.getId(), node);
                    case Way way -> {
                        // Regions can come as a way (a set of nodes), store them into a temporary cache
                        ways.put(way.getId(), way);

                        // If a way is also tagged with the postal code, we will add it's coordinates to the result
                        Optional<Tag> postalCode = way.getTags()
                                .stream()
                                .filter((t) -> t.getKey().equals("postal_code"))
                                .findFirst();
                        if (postalCode.isPresent()) {
                            String postalCodeValue = postalCode.get().getValue();
                            if (StringUtils.isEmpty(include) || include.equals(postalCodeValue)) {
                                List<Coordinate> coordinates = new LinkedList<>();
                                for (WayNode wayNode : way.getWayNodes()) {
                                    Node node = nodes.get(wayNode.getNodeId());
                                    coordinates.add(new Coordinate(node.getLatitude(), node.getLongitude()));
                                    result.computeIfAbsent(postalCodeValue, k -> new ArrayList<>()).addAll(coordinates);
                                }
                            }
                        }
                    }
                    case Relation relation -> {
                        // Regions can also come as a relation (a set of ways and nodes)

                        // If a way is tagged with the postal code, add it's members coordinates to the result
                        Optional<Tag> postalCode = relation.getTags().stream().filter((t) -> t.getKey().equals("postal_code"))
                                .findFirst();
                        if (postalCode.isPresent()) {
                            String postalCodeValue = postalCode.get().getValue();
                            if (StringUtils.isEmpty(include) || include.equals(postalCodeValue)) {
                                List<Coordinate> coordinates = new LinkedList<>();
                                for (RelationMember member : relation.getMembers()) {
                                    switch (member.getMemberType()) {
                                        case Node: {
                                            // The node is part of the relation (uncommon, but to be handled anyway)
                                            Node node = nodes.get(member.getMemberId());
                                            coordinates.add(new Coordinate(node.getLatitude(), node.getLongitude()));
                                            break;
                                        }
                                        case Way: {
                                            // The way is part of the relation, let's add it coordinates to the result
                                            Way way = ways.get(member.getMemberId());
                                            if (way == null) {
                                                Log.debugv("Way {0} in relation {1} not found, skipping", member.getMemberId(), relation.getId());
                                                continue;
                                            }
                                            List<Coordinate> coordinates1 = new ArrayList<>();
                                            for (WayNode wayNode : way.getWayNodes()) {
                                                Node node = nodes.get(wayNode.getNodeId());
                                                coordinates1.add(new Coordinate(node.getLatitude(), node.getLongitude()));
                                            }
                                            appendCoordinates(coordinates1, coordinates);
                                            break;
                                        }
                                        default: {
                                            Log.errorv("Cannot handle {0}", member.getMemberType());
                                            break;
                                        }
                                    }
                                }
                                result.computeIfAbsent(postalCodeValue, k -> new ArrayList<>()).addAll(coordinates);
                            }
                        }
                    }
                    case Bound bound ->
                        // Handle bounds, which are not relevant in the import of ZIP codes
                            Log.debugv("Bounds: {0} / {1} to {2} / {3}", bound.getLeft(), bound.getTop(), bound.getRight(),
                                    bound.getBottom());
                    case null, default -> Log.warnv("Unknown entity type: {0}", entity.getClass());
                }
            }

            @Override
            public void initialize(Map<String, Object> arg0) {
            }

            @Override
            public void complete() {
            }

            @Override
            public void close() {
            }

        };

        reader.setSink(sinkImplementation);
        reader.run();

        return result;
    }

    static void appendCoordinates(List<Coordinate> current, List<Coordinate> coordinates) {
        if (coordinates.isEmpty()) {
            coordinates.addAll(current);
            return;
        }

        // Check if the list starts with the last element of the current, ...
        int lastCoordinate = coordinates.size() - 1;
        if (current.get(0).equals2D(coordinates.get(lastCoordinate))) {
            Log.debugv("Appending {0} current points to {1} coordinate points", current.size(), coordinates.size());
            coordinates.remove(lastCoordinate);
            coordinates.addAll(current);
            return;
        }

        // ... or ends with the first element of current
        int lastCurrent = current.size() - 1;
        if (current.get(lastCurrent).equals2D(coordinates.get(0))) {
            Log.debugv("Prepending {0} current points to {1} coordinate points", current.size(), coordinates.size());
            coordinates.remove(0);
            coordinates.addAll(0, current);
            return;
        }

        // ... or ends with the last element of current
        if (current.get(lastCurrent).equals2D(coordinates.get(lastCoordinate))) {
            Log.debugv("Prepending {0} current points to {1} reversed coordinate points", current.size(), coordinates.size());
            Collections.reverse(coordinates);
            coordinates.remove(0);
            coordinates.addAll(0, current);
            return;
        }

        // ... or starts with the first element of current
        if (current.get(0).equals2D(coordinates.get(0))) {
            Log.debugv("Appending {0} current points to {1} reversed coordinate points", current.size(), coordinates.size());
            Collections.reverse(coordinates);
            coordinates.remove(lastCoordinate);
            coordinates.addAll(current);
            return;
        }

        coordinates.addAll(current);
    }
}
