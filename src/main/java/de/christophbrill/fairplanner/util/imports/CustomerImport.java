package de.christophbrill.fairplanner.util.imports;

import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.fairplanner.persistence.model.ImportProcessEntity;
import de.christophbrill.fairplanner.ui.dto.CustomerImportColumns;
import de.christophbrill.appframework.ui.dto.ImportResult;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.UserTransaction;

@ApplicationScoped
public class CustomerImport {

    @Inject
    CustomerImportXlsx customerImportXlsx;
    @Inject
    CustomerImportCsv customerImportCsv;
    @Inject
    EntityManager entityManager;
    @Inject
    UserTransaction transaction;

    public void runImport(ImportProcessEntity importProcess, boolean removeExisting, boolean skipGeocoding,
                          CustomerImportColumns columns, byte[] data, int sheet, Progress<ImportResult> progress) {


        Log.debug("Consuming import event");

        try {
            ImportResult result = new ImportResult();
            if (removeExisting) {
                transaction.begin();
                result.deleted = entityManager.createQuery("delete from Customer").executeUpdate();
                entityManager.flush();
                transaction.commit();
            }

            if (importProcess.filename.endsWith(".xlsx") || importProcess.filename.endsWith(".xls")) {
                customerImportXlsx.upload(progress, result, data, columns,
                        importProcess.identifierColumn, importProcess.identifierType,
                        skipGeocoding, sheet);
            } else if (importProcess.filename.endsWith(".csv")) {
                customerImportCsv.upload(progress, result, data, columns,
                        importProcess.identifierColumn, importProcess.identifierType,
                        skipGeocoding);
            } else {
                throw new BadArgumentException("Unsupported file ending");
            }
            progress.result = result;
        } catch (Exception e) {
            progress.success = false;
            progress.message = e.getMessage();
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            progress.completed = true;
        }
    }

}
