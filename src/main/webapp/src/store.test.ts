import { beforeEach, expect, test, vi } from 'vitest'
import createFetchMock from 'vitest-fetch-mock'
import { allPermissions } from './store.js'

const fetchMocker = createFetchMock(vi)
fetchMocker.enableMocks()

beforeEach(() => {
  fetchMocker.mockIf(/^\/rest\/permissions.*/, () => {
    return {
      body: '["A", "B", "C"]',
      headers: {
        'Content-Type': 'application/json',
        'Result-Count': '2'
      }
    }
  })
})

test('allPermissions is set', async () => {
  const permissions = await allPermissions()
  expect(permissions).toHaveLength(3)
})

fetchMocker.dontMock()
