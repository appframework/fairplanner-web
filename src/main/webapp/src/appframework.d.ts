/* tslint:disable */
/* eslint-disable */

export interface AbstractDto {
    id: number;
}

export interface BinaryData extends AbstractDto {
    filename: string;
    size: number;
    contentType: string;
}

export interface Credentials {
    username: string;
    password: string;
}

export interface DeletionAction {
    effect: string;
    action: string;
    affected: number;
}

export interface ImportResult {
    skipped: number;
    created: number;
    updated: number;
    deleted: number;
}

export interface Problem {
    title: string;
    status: number;
    detail: string;
}

export interface Progress<T> {
    key: string;
    result: T;
    value: number;
    max: number;
    message: string;
    completed: boolean;
    success: boolean;
}

export interface Role extends AbstractDto {
    name: string;
    permissions: string[];
}

export interface User extends AbstractDto {
    name: string;
    login: string;
    password: string;
    email: string;
    /**
     * @deprecated
     */
    roleIds: number[];
    roles: string[];
    pictureId: number;
}

export interface VersionInformation {
    maven: string;
    git: string;
    buildTimestamp: Date;
}

export interface AbstractMonetaryAmount extends AbstractDto {
    interval: Interval;
    amount: number;
}

export interface Address {
    street: string;
    zip: string;
    city: string;
    lat: number;
    lon: number;
}

export interface Bounds {
    northEast: LatLng;
    southWest: LatLng;
    zoom: number;
}

export interface Column {
    index: number;
    label: string;
}

export interface Contact {
    type: Type;
    value: string;
}

export interface Cost extends AbstractMonetaryAmount {
}

export interface Customer extends AbstractDto {
    name: string;
    address: Address;
    identifiers: Identifier[];
    contactdata: Contact[];
}

export interface CustomerImportColumns {
    name: number[];
    street: number[];
    zip: number[];
    city: number[];
    contact: { [P in Type]?: number[] };
    identifiers: { [index: string]: number[] };
}

export interface Fair extends AbstractDto {
    name: string;
    start: Date;
    end: Date;
}

export interface Identifier {
    type: string;
    value: string;
}

export interface ImportConfiguration {
    columns: Column[];
    configuration: string;
    identifierColumn: number;
    identifierType: string;
    sheet: number;
}

export interface ImportProcess extends AbstractDto {
    filename: string;
    columns: string;
    identifierColumn: number;
    identifierType: string;
}

export interface LatLng {
    lat: number;
    lng: number;
}

export interface Offer extends AbstractDto {
    fairId: number;
    products: Product[];
    productGroups: ProductGroup[];
    reduction: number;
}

export interface Product extends AbstractDto {
    name: string;
    productGroupIds: number[];
    costs: Cost[];
    revenues: Revenue[];
}

export interface ProductGroup extends AbstractDto {
    name: string;
    color: string;
}

export interface Quotation extends AbstractDto {
    customerId: number;
    positions: QuotationProduct[];
}

export interface QuotationProduct extends AbstractDto {
    productId: number;
    reduction: number;
}

export interface Region extends AbstractDto {
    zip: string;
    geom: LatLng[];
}

export interface RegionGroup extends AbstractDto {
    name: string;
    color: string;
    zips: RegionGroupZip[];
    alias: string[];
}

export interface RegionGroupImportColumns {
    from: number[];
    to: number[];
}

export interface RegionGroupRegion {
    group: RegionGroup;
    region: Region;
}

export interface RegionGroupZip {
    from: string;
    to: string;
}

export interface Revenue extends AbstractMonetaryAmount {
}

export type Interval = "SINGLE" | "QUARTERLY" | "MONTHLY";

export type Type = "LANDLINE" | "MOBILE" | "FAX" | "EMAIL" | "WEBSITE";
