import { createRouter, createWebHistory } from 'vue-router'
import { search } from '@/App.vue'
import { isLoggedIn } from '../store'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/login',
      component: () => import('../appframework/views/LoginView.vue')
    },
    {
      path: '/logout',
      component: () => import('../appframework/views/LogoutView.vue')
    },
    {
      path: '/roles',
      component: () => import('../appframework/views/RolesView.vue')
    },
    {
      path: '/roles/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/roles/clone/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/users',
      component: () => import('../appframework/views/UsersView.vue')
    },
    {
      path: '/users/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/users/clone/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/profile',
      component: () => import('../appframework/views/ProfileView.vue')
    },
    {
      path: '/fairs',
      component: () => import('../views/FairsView.vue')
    },
    {
      path: '/fairs/:id',
      component: () => import('../views/FairView.vue')
    },
    {
      path: '/fairs/clone/:id',
      component: () => import('../views/FairView.vue')
    },
    {
      path: '/regions',
      component: () => import('../views/RegionsView.vue')
    },
    {
      path: '/regions/upload',
      component: () => import('../views/RegionUploadView.vue')
    },
    {
      path: '/regions/:id',
      component: () => import('../views/RegionView.vue')
    },
    {
      path: '/regions/clone/:id',
      component: () => import('../views/RegionView.vue')
    },
    {
      path: '/products',
      component: () => import('../views/ProductsView.vue')
    },
    {
      path: '/products/:id',
      component: () => import('../views/ProductView.vue')
    },
    {
      path: '/products/clone/:id',
      component: () => import('../views/ProductView.vue')
    },
    {
      path: '/productgroups',
      component: () => import('../views/ProductGroupsView.vue')
    },
    {
      path: '/productgroups/:id',
      component: () => import('../views/ProductGroupView.vue')
    },
    {
      path: '/productgroups/clone/:id',
      component: () => import('../views/ProductGroupView.vue')
    },
    {
      path: '/regiongroups',
      component: () => import('../views/RegionGroupsView.vue')
    },
    {
      path: '/regiongroups/upload',
      component: () => import('../views/RegionGroupUploadView.vue')
    },
    {
      path: '/regiongroups/:id',
      component: () => import('../views/RegionGroupView.vue')
    },
    {
      path: '/regiongroups/clone/:id',
      component: () => import('../views/RegionGroupView.vue')
    },
    {
      path: '/quotations',
      component: () => import('../views/QuotationsView.vue')
    },
    {
      path: '/quotations/:id',
      component: () => import('../views/QuotationView.vue')
    },
    {
      path: '/quotations/clone/:id',
      component: () => import('../views/QuotationView.vue')
    },
    {
      path: '/customers',
      component: () => import('../views/CustomersView.vue')
    },
    {
      path: '/customers/upload',
      component: () => import('../views/CustomerUploadView.vue')
    },
    {
      path: '/customers/:id',
      component: () => import('../views/CustomerView.vue')
    },
    {
      path: '/customers/clone/:id',
      component: () => import('../views/CustomerView.vue')
    },
    {
      path: '/flow',
      component: () => import('../views/FlowCustomerView.vue')
    }
  ]
})

router.beforeEach((to, from) => {
  // Not logged in, go to login page
  if (!isLoggedIn() && to.path !== '/login') {
    return { path: '/login' }
  }

  // Clear search between modules
  if (to.path === from.path) {
    return
  }
  let currentModule = from.path.split('/')[1]
  if (currentModule === '') {
    currentModule = 'tasks'
  }
  let nextModule = to.path.split('/')[1]
  if (nextModule === '') {
    nextModule = 'tasks'
  }
  if (currentModule !== nextModule) {
    search.value = ''
  }
  return true
})

export default router
