CREATE TABLE fair (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  start date DEFAULT NULL,
  "end" date DEFAULT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE fair
ADD CONSTRAINT FK_fair_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE fair
ADD CONSTRAINT FK_fair_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_fair_creator_IX ON fair (creator_id);
CREATE INDEX FK_fair_modificator_IX ON fair (modificator_id);

CREATE TABLE customer (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  street varchar(255),
  city varchar(255),
  zip varchar(255),
  lat double precision,
  lon double precision,
  PRIMARY KEY (id)
);

ALTER TABLE customer
ADD CONSTRAINT FK_customer_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE customer
ADD CONSTRAINT FK_customer_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_customer_creator_IX ON customer (creator_id);
CREATE INDEX FK_customer_modificator_IX ON customer (modificator_id);

CREATE TABLE appointment (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  customer_id int NOT NULL,
  start timestamp DEFAULT NULL,
  "end" timestamp DEFAULT NULL,
  user_id int DEFAULT NULL,
  fair_id int NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE appointment
ADD CONSTRAINT FK_appointment_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE appointment
ADD CONSTRAINT FK_appointment_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE appointment
ADD CONSTRAINT FK_appointment_customer FOREIGN KEY (customer_id) REFERENCES customer (id);
ALTER TABLE appointment
ADD CONSTRAINT FK_appointment_user FOREIGN KEY (user_id) REFERENCES user_ (id);
ALTER TABLE appointment
ADD CONSTRAINT FK_appointment_fair FOREIGN KEY (fair_id) REFERENCES fair (id);

CREATE INDEX FK_appointment_creator_IX ON appointment (creator_id);
CREATE INDEX FK_appointment_modificator_IX ON appointment (modificator_id);
CREATE INDEX FK_appointment_customer_IX ON appointment (customer_id);
CREATE INDEX FK_appointment_user_IX ON appointment (user_id);
CREATE INDEX FK_appointment_fair_IX ON appointment (fair_id);

CREATE TABLE appointment_note (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  appointment_id int NOT NULL,
  text VARCHAR(65535) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE appointment_note
ADD CONSTRAINT FK_appointmentnote_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE appointment_note
ADD CONSTRAINT FK_appointmentnote_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE appointment_note
ADD CONSTRAINT FK_appointmentnote_appointment FOREIGN KEY (appointment_id) REFERENCES appointment (id);

CREATE INDEX FK_appointmentnote_creator_IX ON appointment_note (creator_id);
CREATE INDEX FK_appointmentnote_modificator_IX ON appointment_note (modificator_id);
CREATE INDEX FK_appointmentnote_appointment_IX ON appointment_note (appointment_id);

CREATE TABLE identifier (
  customer_id int NOT NULL,
  type VARCHAR(255) NOT NULL,
  value VARCHAR(255) NOT NULL
);

ALTER TABLE identifier
ADD CONSTRAINT FK_identifier_customer FOREIGN KEY (customer_id) REFERENCES customer (id);

CREATE INDEX FK_identifier_customer_IX ON identifier (customer_id);

CREATE TABLE contact (
  customer_id int NOT NULL,
  type VARCHAR(31) NOT NULL,
  value VARCHAR(255) NOT NULL
);

ALTER TABLE contact
  ADD CONSTRAINT FK_contact_customer FOREIGN KEY (customer_id) REFERENCES customer (id);

CREATE INDEX FK_contact_customer_IX ON contact (customer_id);

CREATE TABLE product (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE product
ADD CONSTRAINT FK_product_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE product
ADD CONSTRAINT FK_product_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_product_creator_IX ON product (creator_id);
CREATE INDEX FK_product_modificator_IX ON product (modificator_id);

CREATE TABLE product_group (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  color VARCHAR(63) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE product_group
ADD CONSTRAINT FK_productgroup_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE product_group
ADD CONSTRAINT FK_productgroup_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_productgroup_creator_IX ON product_group (creator_id);
CREATE INDEX FK_productgroup_modificator_IX ON product_group (modificator_id);

CREATE TABLE offer (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  fair_id int NOT NULL,
  reduction double precision NOT NULL,
  PRIMARY KEY (id)
);
ALTER TABLE offer
ADD CONSTRAINT FK_offer_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE offer
ADD CONSTRAINT FK_offer_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE offer
ADD CONSTRAINT FK_offer_fair FOREIGN KEY (fair_id) REFERENCES fair (id);

CREATE INDEX FK_offer_creator_IX ON offer (creator_id);
CREATE INDEX FK_offer_modificator_IX ON offer (modificator_id);
CREATE INDEX FK_offer_fair_IX ON offer (fair_id);

CREATE TABLE offer_product (
  offer_id int NOT NULL,
  product_id int NOT NULL
);

ALTER TABLE offer_product
ADD CONSTRAINT FK_offerproduct_offer FOREIGN KEY (offer_id) REFERENCES offer (id);
ALTER TABLE offer_product
ADD CONSTRAINT FK_offerproduct_product FOREIGN KEY (product_id) REFERENCES product (id);

CREATE INDEX FK_offerproduct_offer_IX ON offer_product (offer_id);
CREATE INDEX FK_offerproduct_product_IX ON offer_product (product_id);

CREATE TABLE offer_productgroup (
  offer_id int NOT NULL,
  productgroup_id int NOT NULL
);

ALTER TABLE offer_productgroup
ADD CONSTRAINT FK_offerproductgroup_offer FOREIGN KEY (offer_id) REFERENCES offer (id);
ALTER TABLE offer_productgroup
ADD CONSTRAINT FK_offerproductgroup_productgroup FOREIGN KEY (productgroup_id) REFERENCES product_group (id);

CREATE INDEX FK_offerproductgroup_offer_IX ON offer_productgroup (offer_id);
CREATE INDEX FK_offerproductgroup_product_IX ON offer_productgroup (productgroup_id);

CREATE TABLE quotation (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  customer_id int NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE quotation
  ADD CONSTRAINT FK_quotation_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE quotation
  ADD CONSTRAINT FK_quotation_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE quotation
  ADD CONSTRAINT FK_quotation_customer FOREIGN KEY (customer_id) REFERENCES customer (id);

CREATE INDEX FK_quotation_creator_IX ON quotation (creator_id);
CREATE INDEX FK_quotation_modificator_IX ON quotation (modificator_id);
CREATE INDEX FK_quotation_customer_IX ON quotation (customer_id);

CREATE TABLE quotation_product (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  quotation_id int NOT NULL,
  product_id int NOT NULL,
  reduction double precision NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE quotation_product
  ADD CONSTRAINT FK_quotationproduct_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE quotation_product
  ADD CONSTRAINT FK_quotationproduct_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE quotation_product
  ADD CONSTRAINT FK_quotationproduct_quotation FOREIGN KEY (quotation_id) REFERENCES quotation (id);
ALTER TABLE quotation_product
  ADD CONSTRAINT FK_quotationproduct_product FOREIGN KEY (product_id) REFERENCES product (id);

CREATE INDEX FK_quotationproduct_creator_IX ON quotation_product (creator_id);
CREATE INDEX FK_quotationproduct_modificator_IX ON quotation_product (modificator_id);
CREATE INDEX FK_quotationproduct_quotation_IX ON quotation_product (quotation_id);
CREATE INDEX FK_quotationproduct_product_IX ON quotation_product (product_id);

CREATE TABLE product_productgroup (
  product_id int NOT NULL,
  productgroup_id int NOT NULL
);

ALTER TABLE product_productgroup
ADD CONSTRAINT FK_productproductgroup_product FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE product_productgroup
ADD CONSTRAINT FK_productproductgroup_productgroup FOREIGN KEY (productgroup_id) REFERENCES product_group (id);

CREATE INDEX FK_productproductgroup_product_IX ON product_productgroup (product_id);
CREATE INDEX FK_productproductgroup_productgroup_IX ON product_productgroup (productgroup_id);

CREATE TABLE cost (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  product_id int NOT NULL,
  interval VARCHAR(20) NOT NULL,
  amount double precision NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE cost
ADD CONSTRAINT FK_cost_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE cost
ADD CONSTRAINT FK_cost_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE cost
ADD CONSTRAINT FK_cost_product FOREIGN KEY (product_id) REFERENCES product (id);

CREATE INDEX FK_cost_creator_IX ON cost (creator_id);
CREATE INDEX FK_cost_modificator_IX ON cost (modificator_id);
CREATE INDEX FK_cost_product_IX ON cost (product_id);

CREATE TABLE revenue (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  product_id int NOT NULL,
  interval VARCHAR(20) NOT NULL,
  amount double precision NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE revenue
ADD CONSTRAINT FK_revenue_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE revenue
ADD CONSTRAINT FK_revenue_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE revenue
ADD CONSTRAINT FK_revenue_product FOREIGN KEY (product_id) REFERENCES product (id);

CREATE INDEX FK_revenue_creator_IX ON revenue (creator_id);
CREATE INDEX FK_revenue_modificator_IX ON revenue (modificator_id);
CREATE INDEX FK_revenue_product_IX ON revenue (product_id);

INSERT INTO role_permissions(role_id,permission)
 VALUES ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_FAIRS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_PRODUCTS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_PRODUCTGROUPS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_CUSTOMERS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'USE_FLOW'),
        ((SELECT id FROM role WHERE name = 'Users'),'USE_FLOW');

CREATE TABLE coordinate_cache (
  id serial NOT NULL,
  query varchar(511),
  lat double precision,
  lon double precision,
  PRIMARY KEY (id)
);