CREATE TABLE regiongroup_alias
(
    regiongroup_id bigint NOT NULL,
    alias    varchar(255) DEFAULT NULL,
    CONSTRAINT FK_regiongroupalias_regiongroup FOREIGN KEY (regiongroup_id) REFERENCES regiongroup (id)
);

CREATE INDEX FK_regiongroupalias_regiongroup_IX ON regiongroup_alias (regiongroup_id);