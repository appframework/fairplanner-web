CREATE TABLE regiongroup (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  name varchar(255),
  color VARCHAR(63) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE regiongroup
ADD CONSTRAINT FK_regiongroup_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE regiongroup
ADD CONSTRAINT FK_regiongroup_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_regiongroup_creator_IX ON regiongroup (creator_id);
CREATE INDEX FK_regiongroup_modificator_IX ON regiongroup (modificator_id);

CREATE TABLE regiongroup_zip (
  regiongroup_id int not null,
  zip_from varchar(255) not null,
  zip_to varchar(255) not null
);

ALTER TABLE regiongroup_zip
ADD CONSTRAINT FK_regiongroupzip_regiongroup FOREIGN KEY (regiongroup_id) REFERENCES regiongroup (id);

CREATE INDEX IX_regiongroupzip_zipfrom ON regiongroup_zip (zip_from);
CREATE INDEX IX_regiongroupzip_zipto ON regiongroup_zip (zip_to);
CREATE INDEX FK_regiongroupzip_regiongroup_IX ON regiongroup_zip (regiongroup_id);

CREATE INDEX IX_region_zip ON region(zip);