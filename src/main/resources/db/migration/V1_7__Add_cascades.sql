ALTER TABLE identifier
DROP CONSTRAINT FK_identifier_customer;

ALTER TABLE identifier
ADD CONSTRAINT FK_identifier_customer FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE CASCADE;

ALTER TABLE contact
DROP CONSTRAINT FK_contact_customer;

ALTER TABLE contact
ADD CONSTRAINT FK_contact_customer FOREIGN KEY (customer_id) REFERENCES customer (id);
