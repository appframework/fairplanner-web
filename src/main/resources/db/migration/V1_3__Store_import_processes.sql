CREATE TABLE importprocess (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  filename VARCHAR(255) NOT NULL,
  columns VARCHAR(1023) NOT NULL,
  identifier_column int NOT NULL,
  identifier_type VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE importprocess
ADD CONSTRAINT FK_importprocess_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE importprocess
ADD CONSTRAINT FK_importprocess_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_importprocess_creator_IX ON importprocess (creator_id);
CREATE INDEX FK_importprocess_modificator_IX ON importprocess (modificator_id);
