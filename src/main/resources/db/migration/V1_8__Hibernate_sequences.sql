create sequence appointment_note_SEQ INCREMENT 50;
select setval('appointment_note_SEQ', (select max(id) from appointment_note));

create sequence appointment_SEQ INCREMENT 50;
select setval('appointment_SEQ', (select max(id) from appointment));

create sequence binary_data_SEQ INCREMENT 50;
select setval('binary_data_SEQ', (select max(id) from binary_data));

create sequence cost_SEQ INCREMENT 50;
select setval('cost_SEQ', (select max(id) from cost));

create sequence customer_SEQ INCREMENT 50;
select setval('customer_SEQ', (select max(id) from customer));

create sequence fair_SEQ INCREMENT 50;
select setval('fair_SEQ', (select max(id) from fair));

create sequence importprocess_SEQ INCREMENT 50;
select setval('importprocess_SEQ', (select max(id) from importprocess));

create sequence offer_SEQ INCREMENT 50;
select setval('offer_SEQ', (select max(id) from offer));

create sequence product_SEQ INCREMENT 50;
select setval('product_SEQ', (select max(id) from product));

create sequence product_group_SEQ INCREMENT 50;
select setval('product_group_SEQ', (select max(id) from product_group));

create sequence quotation_SEQ INCREMENT 50;
select setval('quotation_SEQ', (select max(id) from quotation));

create sequence quotation_product_SEQ INCREMENT 50;
select setval('quotation_product_SEQ', (select max(id) from quotation_product));

create sequence region_SEQ INCREMENT 50;
select setval('region_SEQ', (select max(id) from region));

create sequence regiongroup_SEQ INCREMENT 50;
select setval('regiongroup_SEQ', (select max(id) from regiongroup));

create sequence revenue_SEQ INCREMENT 50;
select setval('revenue_SEQ', (select max(id) from revenue));

create sequence role_SEQ INCREMENT 50;
select setval('role_SEQ', (select max(id) from role));

create sequence user__SEQ INCREMENT 50;
select setval('user__SEQ', (select max(id) from user_));

drop sequence if exists hibernate_sequence;