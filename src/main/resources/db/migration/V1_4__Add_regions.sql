CREATE TABLE region (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  zip varchar(255),
  PRIMARY KEY (id)
);

SELECT AddGeometryColumn('region', 'geom', 4326, 'POLYGON', 2);

ALTER TABLE region
ADD CONSTRAINT FK_region_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE region
ADD CONSTRAINT FK_region_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE INDEX FK_region_creator_IX ON region (creator_id);
CREATE INDEX FK_region_modificator_IX ON region (modificator_id);


INSERT INTO role_permissions(role_id,permission)
 VALUES ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_REGIONS');
