ALTER TABLE appointment
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN customer_id TYPE bigint,
    ALTER COLUMN user_id TYPE bigint,
    ALTER COLUMN fair_id TYPE bigint;
ALTER TABLE appointment_note
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN appointment_id TYPE bigint;
ALTER TABLE binary_data
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE contact
    ALTER COLUMN customer_id TYPE bigint;
ALTER TABLE cost
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN product_id TYPE bigint;
ALTER TABLE customer
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE fair
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE identifier
    ALTER COLUMN customer_id TYPE bigint;
ALTER TABLE importprocess
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE offer
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN fair_id TYPE bigint;
ALTER TABLE offer_product
    ALTER COLUMN offer_id TYPE bigint,
    ALTER COLUMN product_id TYPE bigint;
ALTER TABLE offer_productgroup
    ALTER COLUMN offer_id TYPE bigint,
    ALTER COLUMN productgroup_id TYPE bigint;
ALTER TABLE product
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE product_group
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE product_productgroup
    ALTER COLUMN product_id TYPE bigint,
    ALTER COLUMN productgroup_id TYPE bigint;
ALTER TABLE quotation
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN customer_id TYPE bigint;
ALTER TABLE quotation_product
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN quotation_id TYPE bigint,
    ALTER COLUMN product_id TYPE bigint;
ALTER TABLE region
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE regiongroup
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE regiongroup_zip
    ALTER COLUMN regiongroup_id TYPE bigint;
ALTER TABLE revenue
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN product_id TYPE bigint;
ALTER TABLE role
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE role_permissions
    ALTER COLUMN role_id TYPE bigint;
ALTER TABLE user_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN picture_id TYPE bigint;
ALTER TABLE user_role
    ALTER COLUMN user_id TYPE bigint,
    ALTER COLUMN role_id TYPE bigint;
