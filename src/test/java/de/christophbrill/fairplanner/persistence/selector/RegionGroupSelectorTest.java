package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class RegionGroupSelectorTest extends AbstractResourceSelectorTest<RegionGroupEntity> {

    @Override
    protected RegionGroupSelector getSelector() {
        return new RegionGroupSelector(em);
    }

}
