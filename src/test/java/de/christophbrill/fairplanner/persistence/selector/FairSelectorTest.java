package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class FairSelectorTest extends AbstractResourceSelectorTest<FairEntity> {

	@Override
	protected FairSelector getSelector() {
		return new FairSelector(em);
	}

}
