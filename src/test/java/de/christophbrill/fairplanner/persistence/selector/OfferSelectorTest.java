package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.OfferEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class OfferSelectorTest extends AbstractResourceSelectorTest<OfferEntity> {

    @Override
    protected OfferSelector getSelector() {
        return new OfferSelector(em);
    }

}
