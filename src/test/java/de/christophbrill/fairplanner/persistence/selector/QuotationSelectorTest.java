package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.QuotationEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class QuotationSelectorTest extends AbstractResourceSelectorTest<QuotationEntity> {

    @Override
    protected QuotationSelector getSelector() {
        return new QuotationSelector(em);
    }

}
