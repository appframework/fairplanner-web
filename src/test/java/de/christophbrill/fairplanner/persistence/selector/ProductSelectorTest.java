package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.ProductEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ProductSelectorTest extends AbstractResourceSelectorTest<ProductEntity> {

    @Override
    protected ProductSelector getSelector() {
        return new ProductSelector(em);
    }

}
