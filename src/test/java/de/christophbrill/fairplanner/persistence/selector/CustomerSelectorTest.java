package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class CustomerSelectorTest extends AbstractResourceSelectorTest<CustomerEntity> {

	@Override
	protected CustomerSelector getSelector() {
		return new CustomerSelector(em);
	}

}
