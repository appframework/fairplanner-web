package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.ImportProcessEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ImportProcessSelectorTest extends AbstractResourceSelectorTest<ImportProcessEntity> {

    @Override
    protected ImportProcessSelector getSelector() {
        return new ImportProcessSelector(em);
    }

}
