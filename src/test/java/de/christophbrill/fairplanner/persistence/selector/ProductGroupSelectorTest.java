package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ProductGroupSelectorTest extends AbstractResourceSelectorTest<ProductGroupEntity> {

    @Override
    protected ProductGroupSelector getSelector() {
        return new ProductGroupSelector(em);
    }

}
