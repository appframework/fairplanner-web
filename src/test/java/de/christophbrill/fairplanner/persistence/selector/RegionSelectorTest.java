package de.christophbrill.fairplanner.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.fairplanner.persistence.model.RegionEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class RegionSelectorTest extends AbstractResourceSelectorTest<RegionEntity> {

    @Override
    protected RegionSelector getSelector() {
        return new RegionSelector(em);
    }

}
