package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.RegionGroupEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class RegionGroupDaoTest extends AbstractDaoCRUDTest<RegionGroupEntity> {

    @Override
    protected RegionGroupEntity createFixture() {
        RegionGroupEntity regiongroup = new RegionGroupEntity();
        regiongroup.name = UUID.randomUUID().toString();
        regiongroup.color = "#ff00ff";
        return regiongroup;
    }

    @Override
    protected void modifyFixture(RegionGroupEntity regiongroup) {
        regiongroup.name = UUID.randomUUID().toString();
    }

    @Override
    protected RegionGroupEntity findById(Long id) {
        return RegionGroupEntity.findById(id);
    }

    @Override
    protected Long count() {
        return RegionGroupEntity.count();
    }

}
