package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.ProductGroupEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class ProductGroupDaoTest extends AbstractDaoCRUDTest<ProductGroupEntity> {

    @Override
    protected ProductGroupEntity createFixture() {
        ProductGroupEntity productgroup = new ProductGroupEntity();
        productgroup.name = UUID.randomUUID().toString();
        productgroup.color = "#ff0000";
        return productgroup;
    }

    @Override
    protected void modifyFixture(ProductGroupEntity productgroup) {
        productgroup.name = UUID.randomUUID().toString();
    }

    @Override
    protected ProductGroupEntity findById(Long id) {
        return ProductGroupEntity.findById(id);
    }

    @Override
    protected Long count() {
        return ProductGroupEntity.count();
    }

}
