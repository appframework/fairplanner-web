package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.RegionEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class RegionDaoTest extends AbstractDaoCRUDTest<RegionEntity> {

    @Override
    protected RegionEntity createFixture() {
        RegionEntity region = new RegionEntity();
        region.zip = UUID.randomUUID().toString();
        return region;
    }

    @Override
    protected void modifyFixture(RegionEntity region) {
        region.zip = UUID.randomUUID().toString();
    }

    @Override
    protected RegionEntity findById(Long id) {
        return RegionEntity.findById(id);
    }

    @Override
    protected Long count() {
        return RegionEntity.count();
    }

}
