package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import de.christophbrill.fairplanner.persistence.model.OfferEntity;
import de.christophbrill.fairplanner.persistence.model.ProductEntity;
import de.christophbrill.fairplanner.persistence.model.QuotationEntity;
import de.christophbrill.fairplanner.persistence.model.QuotationProductEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class QuotationDaoTest extends AbstractDaoCRUDTest<QuotationEntity> {

    @Override
    protected QuotationEntity createFixture() {
        var customer = new CustomerDaoTest()
                .createFixture();
        customer.persistAndFlush();

        var product = new ProductDaoTest()
                .createFixture();
        product.persistAndFlush();

        QuotationEntity quotation = new QuotationEntity();
        quotation.customer = customer;
        QuotationProductEntity quotationProductEntity = new QuotationProductEntity();
        quotationProductEntity.quotation = quotation;
        quotationProductEntity.product = product;
        quotationProductEntity.reduction = 0.1;
        quotation.positions.add(quotationProductEntity);
        return quotation;
    }

    @Override
    protected void modifyFixture(QuotationEntity quotation) {
        quotation.positions.get(0).reduction = 0.2;
    }

    @Override
    protected QuotationEntity findById(Long id) {
        return QuotationEntity.findById(id);
    }

    @Override
    protected Long count() {
        return QuotationEntity.count();
    }

}
