package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.OfferEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class OfferDaoTest extends AbstractDaoCRUDTest<OfferEntity> {

    @Override
    protected OfferEntity createFixture() {
        var fair = new FairDaoTest()
                .createFixture();
        fair.persistAndFlush();

        OfferEntity offer = new OfferEntity();
        offer.fair = fair;
        offer.reduction = 0.1;
        return offer;
    }

    @Override
    protected void modifyFixture(OfferEntity offer) {
        offer.reduction = 0.2;
    }

    @Override
    protected OfferEntity findById(Long id) {
        return OfferEntity.findById(id);
    }

    @Override
    protected Long count() {
        return OfferEntity.count();
    }

}
