package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.ProductEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class ProductDaoTest extends AbstractDaoCRUDTest<ProductEntity> {

    @Override
    protected ProductEntity createFixture() {
        ProductEntity product = new ProductEntity();
        product.name = UUID.randomUUID().toString();
        return product;
    }

    @Override
    protected void modifyFixture(ProductEntity product) {
        product.name = UUID.randomUUID().toString();
    }

    @Override
    protected ProductEntity findById(Long id) {
        return ProductEntity.findById(id);
    }

    @Override
    protected Long count() {
        return ProductEntity.count();
    }

}
