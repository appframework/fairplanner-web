package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.FairEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.time.LocalDate;
import java.util.UUID;

@QuarkusTest
public class FairDaoTest extends AbstractDaoCRUDTest<FairEntity> {

    @Override
    protected FairEntity createFixture() {
        FairEntity fair = new FairEntity();
        fair.name = UUID.randomUUID().toString();
        fair.start = LocalDate.now();
        fair.end = LocalDate.now();
        return fair;
    }

    @Override
    protected void modifyFixture(FairEntity fair) {
        fair.name = UUID.randomUUID().toString();
    }

    @Override
    protected FairEntity findById(Long id) {
        return FairEntity.findById(id);
    }

    @Override
    protected Long count() {
        return FairEntity.count();
    }

}
