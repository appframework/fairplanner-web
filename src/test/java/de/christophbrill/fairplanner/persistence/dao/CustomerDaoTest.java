package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.CustomerEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class CustomerDaoTest extends AbstractDaoCRUDTest<CustomerEntity> {

    @Override
    protected CustomerEntity createFixture() {
        CustomerEntity customer = new CustomerEntity();
        customer.name = UUID.randomUUID().toString();
        return customer;
    }

    @Override
    protected void modifyFixture(CustomerEntity fair) {
        fair.name = UUID.randomUUID().toString();
    }

    @Override
    protected CustomerEntity findById(Long id) {
        return CustomerEntity.findById(id);
    }

    @Override
    protected Long count() {
        return CustomerEntity.count();
    }

}
