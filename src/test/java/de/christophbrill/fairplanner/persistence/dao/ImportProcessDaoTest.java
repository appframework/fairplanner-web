package de.christophbrill.fairplanner.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.fairplanner.persistence.model.ImportProcessEntity;
import io.quarkus.test.junit.QuarkusTest;

import java.util.UUID;

@QuarkusTest
public class ImportProcessDaoTest extends AbstractDaoCRUDTest<ImportProcessEntity> {

    @Override
    protected ImportProcessEntity createFixture() {
        ImportProcessEntity importprocess = new ImportProcessEntity();
        importprocess.filename = UUID.randomUUID().toString();
        importprocess.columns = "{}";
        importprocess.identifierType = "RANDOM";
        importprocess.identifierColumn = 5;
        return importprocess;
    }

    @Override
    protected void modifyFixture(ImportProcessEntity importprocess) {
        importprocess.filename = UUID.randomUUID().toString();
    }

    @Override
    protected ImportProcessEntity findById(Long id) {
        return ImportProcessEntity.findById(id);
    }

    @Override
    protected Long count() {
        return ImportProcessEntity.count();
    }

}
