package de.christophbrill.fairplanner.ui.rest;

import de.christophbrill.fairplanner.ui.dto.Column;
import de.christophbrill.fairplanner.ui.dto.Customer;
import de.christophbrill.fairplanner.ui.dto.ImportConfiguration;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import jakarta.ws.rs.core.MediaType;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestHTTPEndpoint(CustomerImportService.class)
@TestSecurity(user = "tom", roles = {"ADMIN_CUSTOMERS", "SHOW_CUSTOMERS"})
public class CustomerImportServiceTest {

    // For testing purposes
    /*static {
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }*/

    @Test
    public void testCheckForUploadCsv() {
        String csv = "ID;DUMMY1;DUMMY2";

        Response response = given()
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .multiPart("file", "DUMMY.CSV", csv.getBytes())
                .multiPart("filename", "DUMMY.csv")
                .when()
                .post()
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .extract().response();

        ImportConfiguration configuration = response.as(ImportConfiguration.class);

        List<Column> columns = configuration.columns;
        assertThat(columns).hasSize(3);
        assertThat(columns.get(0)).isEqualTo(new Column(0, "ID"));
        assertThat(columns.get(1)).isEqualTo(new Column(1, "DUMMY1"));
        assertThat(columns.get(2)).isEqualTo(new Column(2, "DUMMY2"));
    }

    @Test
    @Disabled("ARJUNA016051: thread is already associated with a transaction!")
    public void testUploadCsv() throws InterruptedException {
        long before = Long.parseLong(given()
                .basePath("/")
                .when()
                .get("/rest/customer?limit=1")
                .header("Result-Count"));


        String csv = "ID;NAME;DUMMY2\n1000;Name;Dummyvalue";

        String token = given()
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .multiPart("file", "DUMMY.CSV", csv.getBytes())
                .multiPart("filename", "DUMMY.csv")
                .param("columns", "{\"name\": [1]}")
                .param("identifier", "0")
                .param("identifierType", "TEST")
                .param("removeExisting", "false")
                .when()
                .post("import")
                .then()
                .statusCode(200)
                .contentType(ContentType.TEXT)
                .extract().response().body().asString();
        assertThat(token).isNotNull();

        // TODO Actually check the status using the token
        Thread.sleep(4000);

        long after = Long.parseLong(given()
                .basePath("/")
                .when()
                .get("/rest/customer?limit=1")
                .header("Result-Count"));
        assertThat(after).isEqualTo(before + 1);


        var customer = given()
                .contentType(ContentType.JSON)
                .when()
                .get("/rest/customer/{type}/{identifier}", "TEST", "1000")
                .then().statusCode(200)
                .extract()
                .as(Customer.class);

        assertThat(customer).isNotNull();
        assertThat(customer.name).isEqualTo("Name");
    }

}
