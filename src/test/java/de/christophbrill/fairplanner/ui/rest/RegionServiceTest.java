package de.christophbrill.fairplanner.ui.rest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import de.christophbrill.fairplanner.ui.dto.LatLng;
import de.christophbrill.fairplanner.ui.dto.Region;

import static org.assertj.core.api.Assertions.assertThat;

public class RegionServiceTest {

    @Test
    public void testReduce() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method reduceMethod = RegionService.class.getDeclaredMethod("reduce", Region.class, Integer.class);
        reduceMethod.setAccessible(true);
        Region region = new Region();
        region.geom = new ArrayList<>(Arrays.asList(new LatLng(52.000, 6.000), new LatLng(52.100, 6.100), new LatLng(52.1000001, 6.1000001)));
        Region reduced = (Region) reduceMethod.invoke(null, region, 7);
        assertThat(reduced.geom).hasSize(2);
    }
}
