package de.christophbrill.fairplanner.util.imports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.dto.ImportResult;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;

import static org.assertj.core.api.Assertions.assertThat;

class RegionImportOsmPbfTest {

    @Test
    @Disabled("Only for file validation tests, not an actual unit test")
    void test() {
        ImportResult result = new ImportResult();
        Progress<ImportResult> progress = new Progress<>();
        new RegionImportOsmPbf().importPbf(getClass().getResourceAsStream("/plz-gebiete.osm.pbf"), null, progress, result);
    }

    @Test
    void testMergeWays_twoWays() {

        List<Coordinate> a = new ArrayList<>(Arrays.asList(new Coordinate(1, 1), new Coordinate(1, 2)));
        List<Coordinate> b = new ArrayList<>(Arrays.asList(new Coordinate(1, 2), new Coordinate(2, 1)));

        RegionImportOsmPbf.appendCoordinates(a, b);

        assertThat(b).hasSize(3);
    }

    @Test
    void testMergeWays_twoWaysReverse() {

        List<Coordinate> a = new ArrayList<>(Arrays.asList(new Coordinate(1, 1), new Coordinate(1, 2)));
        List<Coordinate> b = new ArrayList<>(Arrays.asList(new Coordinate(1, 2), new Coordinate(1, 1)));

        RegionImportOsmPbf.appendCoordinates(a, b);

        assertThat(b).hasSize(3);
    }

    @Test
    void testMergeWays_threeWays() {

        List<Coordinate> a = new ArrayList<>(Arrays.asList(new Coordinate(1, 1), new Coordinate(1, 2)));
        List<Coordinate> b = new ArrayList<>(Arrays.asList(new Coordinate(1, 2), new Coordinate(2, 1)));
        List<Coordinate> c = new ArrayList<>(Arrays.asList(new Coordinate(2, 1), new Coordinate(2, 2)));

        RegionImportOsmPbf.appendCoordinates(a, b);
        RegionImportOsmPbf.appendCoordinates(b, c);

        assertThat(c).hasSize(4);
    }

    @Test
    void testMergeWays_fourWays() {

        List<Coordinate> a = new ArrayList<>(Arrays.asList(new Coordinate(1, 1), new Coordinate(1, 2)));
        List<Coordinate> b = new ArrayList<>(Arrays.asList(new Coordinate(1, 2), new Coordinate(2, 1)));
        List<Coordinate> c = new ArrayList<>(Arrays.asList(new Coordinate(2, 1), new Coordinate(2, 2)));
        List<Coordinate> d = new ArrayList<>(Arrays.asList(new Coordinate(1, 2), new Coordinate(2, 2)));

        RegionImportOsmPbf.appendCoordinates(a, b);
        RegionImportOsmPbf.appendCoordinates(b, c);
        RegionImportOsmPbf.appendCoordinates(c, d);

        assertThat(d).hasSize(5);
    }

}
