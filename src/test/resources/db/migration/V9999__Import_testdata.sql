TRUNCATE TABLE
    appointment_note,
    appointment,
    binary_data,
    contact,
    cost,
    customer,
    fair,
    identifier,
    importprocess,
    offer,
    offer_product,
    offer_productgroup,
    product,
    product_group,
    product_productgroup,
    quotation,
    quotation_product,
    region,
    regiongroup,
    regiongroup_zip,
    revenue,
    role,
    role_permissions,
    user_,
    user_role;

INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (1,'Tom Tester','tom','ab4d8d2a5f480a137067da17100271cd176607a1','tom@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (2,'XXX','xxx','b60d121b438a380c343d5ec3c2037564b82ffef3','xxx@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (3,'YYY','yyy','186154712b2d5f6791d85b9a0987b98fa231779c','yyy@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE user__SEQ RESTART WITH 4;

INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (1,'Administrators',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (2,'Users',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (3,'No Permissions',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE role_SEQ RESTART WITH 4;

INSERT INTO role_permissions(role_id,permission) VALUES (1,'SHOW_REGIONS'), (1,'ADMIN_REGIONS'), (1,'ADMIN_USERS'), (1,'SHOW_USERS'), (1,'ADMIN_ROLES'), (1,'SHOW_ROLES');
INSERT INTO role_permissions(role_id,permission) VALUES (2,'SHOW_TASKS');

INSERT INTO user_role(user_id,role_id) VALUES (1,1);
INSERT INTO user_role(user_id,role_id) VALUES (2,2);
INSERT INTO user_role(user_id,role_id) VALUES (3,3);

INSERT INTO customer (id, created, modified, name, creator_id, modificator_id) VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Customer 1', 1, NULL);
INSERT INTO customer (id, created, modified, name, creator_id, modificator_id) VALUES (2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Customer 2', 1, NULL);
INSERT INTO customer (id, created, modified, name, creator_id, modificator_id) VALUES (3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Customer 3', 1, NULL);
ALTER SEQUENCE customer_SEQ RESTART WITH 4;

INSERT INTO identifier(customer_id, type, value) VALUES (1, 'ext_id', '1234');
INSERT INTO identifier(customer_id, type, value) VALUES (1, 'random', '4 (chosen by fair dice roll)');
INSERT INTO identifier(customer_id, type, value) VALUES (2, 'ext_id', '2345');
INSERT INTO identifier(customer_id, type, value) VALUES (3, 'ext_id', '3456');

INSERT INTO fair (id, created, modified, name, start, "end", creator_id, modificator_id) VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'MEDICA 2017', '2017-11-13', '2017-11-16', 1, NULL);
INSERT INTO fair (id, created, modified, name, start, "end", creator_id, modificator_id) VALUES (2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'MEDICA 2018', '2018-11-12', '2018-11-15', 1, NULL);
INSERT INTO fair (id, created, modified, name, start, "end", creator_id, modificator_id) VALUES (3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Medizin 2018', '2018-01-26', '2018-01-28', 1, NULL);
ALTER SEQUENCE fair_SEQ RESTART WITH 4;