## [1.2.28](https://gitlab.com/appframework/fairplanner-web/compare/1.2.27...1.2.28) (2025-02-26)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.26 ([ceabc23](https://gitlab.com/appframework/fairplanner-web/commit/ceabc23dbeb7e85a149c2eff4fcd5b7f7c6ad0c5))
* **deps:** update dependency bootstrap-vue-next to v0.26.28 ([b1d8c3e](https://gitlab.com/appframework/fairplanner-web/commit/b1d8c3e9a19df27d87dea703374877de0d69a02e))
* **deps:** update dependency bootstrap-vue-next to v0.26.30 ([dd882fe](https://gitlab.com/appframework/fairplanner-web/commit/dd882feeacd8c49d59e7fb88c0183632aae8e6ae))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.6.final ([a084726](https://gitlab.com/appframework/fairplanner-web/commit/a0847263678dbb0746cfd905e57774cd2de90e9f))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.7.final ([0c1f54e](https://gitlab.com/appframework/fairplanner-web/commit/0c1f54e1585b65295a31b8f59405547598136cf4))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.8.final ([60944a9](https://gitlab.com/appframework/fairplanner-web/commit/60944a94db3e35c6d0f9032a2bb62d835b2d6ef7))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.9.final ([f7cdaeb](https://gitlab.com/appframework/fairplanner-web/commit/f7cdaebe380bfa99f053646e0c1c8f8f970ec8ca))
* **deps:** update dependency vue-i18n to v11.1.0 ([3d76812](https://gitlab.com/appframework/fairplanner-web/commit/3d76812fb3e354fc33f8ac68ceca4030c0b8c763))
* **deps:** update dependency vue-i18n to v11.1.1 ([45ad36a](https://gitlab.com/appframework/fairplanner-web/commit/45ad36a162240caefdbe6499b90eb4560d61e753))
* **deps:** update quarkus.platform.version to v3.18.1 ([2cb9d26](https://gitlab.com/appframework/fairplanner-web/commit/2cb9d265d2b85bf721a7187b6cd099499ed7d352))
* **deps:** update quarkus.platform.version to v3.18.2 ([a6e6659](https://gitlab.com/appframework/fairplanner-web/commit/a6e6659434f989fcd774450b31c794ec00d5dc66))
* **deps:** update quarkus.platform.version to v3.18.3 ([b374c04](https://gitlab.com/appframework/fairplanner-web/commit/b374c04623d4d30b866b027190db0f0fbf95ae9e))

## [1.2.27](https://gitlab.com/appframework/fairplanner-web/compare/1.2.26...1.2.27) (2025-01-28)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.5.0 ([2e04c81](https://gitlab.com/appframework/fairplanner-web/commit/2e04c8166b812bc41bd5452275ebef10aa6e7f42))
* **deps:** update dependency bootstrap-vue-next to v0.26.21 ([65d68d7](https://gitlab.com/appframework/fairplanner-web/commit/65d68d7740fc871ac236c70a130bc9fdc1ee3cbe))
* **deps:** update dependency bootstrap-vue-next to v0.26.22 ([c6850dd](https://gitlab.com/appframework/fairplanner-web/commit/c6850dd30a9e50e394e635401412ff568bf8b48d))
* **deps:** update dependency org.geotools:gt-shapefile to v32.2 ([87c0886](https://gitlab.com/appframework/fairplanner-web/commit/87c0886fdcea2c348e0f1c337cf0f28e7da254e5))
* **deps:** update dependency primelocale to v1.5.0 ([6d7481e](https://gitlab.com/appframework/fairplanner-web/commit/6d7481ef1b7802605d1616c6e20208ffb4dd8730))
* **deps:** update quarkus.platform.version to v3.18.0 ([4186ce7](https://gitlab.com/appframework/fairplanner-web/commit/4186ce7d05c6dec140b38be9474ba657e47bc719))

## [1.2.26](https://gitlab.com/appframework/fairplanner-web/compare/1.2.25...1.2.26) (2025-01-22)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.20 ([cecdb17](https://gitlab.com/appframework/fairplanner-web/commit/cecdb17087330e16da5d0f56a7404510ca4c5614))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.5.final ([c50eb1d](https://gitlab.com/appframework/fairplanner-web/commit/c50eb1d445e18eb8c2b68ca87ea0f40a8cc2c32a))
* **deps:** update dependency primelocale to v1.4.0 ([f459e6d](https://gitlab.com/appframework/fairplanner-web/commit/f459e6d2154fd5beecedb6adf0a19207229b29c9))
* **deps:** update quarkus.platform.version to v3.17.7 ([69ec8b7](https://gitlab.com/appframework/fairplanner-web/commit/69ec8b74bbf585606c362b861bdcf0584da63256))

## [1.2.25](https://gitlab.com/appframework/fairplanner-web/compare/1.2.24...1.2.25) (2025-01-15)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.4.0 ([05ba4a8](https://gitlab.com/appframework/fairplanner-web/commit/05ba4a880e9dc936444018f3ceeb2719f26481f9))
* **deps:** update dependency org.apache.commons:commons-csv to v1.13.0 ([482cffb](https://gitlab.com/appframework/fairplanner-web/commit/482cffbfdc2fb08ef74fb4a3006c60485428afe4))
* **deps:** update dependency org.apache.poi:poi-ooxml to v5.4.0 ([54cfcb5](https://gitlab.com/appframework/fairplanner-web/commit/54cfcb50f4936ee3a9f2c90859a6afcb501f41f5))
* **deps:** update quarkus.platform.version to v3.17.6 ([3d166d1](https://gitlab.com/appframework/fairplanner-web/commit/3d166d1a768c658d7e7020e41d7bb89e54c87eac))

## [1.2.24](https://gitlab.com/appframework/fairplanner-web/compare/1.2.23...1.2.24) (2025-01-09)


### Bug Fixes

* **deps:** update dependency vue-i18n to v11 ([897ef64](https://gitlab.com/appframework/fairplanner-web/commit/897ef64cfdbd2835d5b6fb363db670f6372743bf))

## [1.2.23](https://gitlab.com/appframework/fairplanner-web/compare/1.2.22...1.2.23) (2025-01-08)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.3.0 ([7296393](https://gitlab.com/appframework/fairplanner-web/commit/7296393777a24ae3218e1977014ef183dfbeed2c))
* **deps:** update dependency bootstrap-vue-next to v0.26.19 ([211b209](https://gitlab.com/appframework/fairplanner-web/commit/211b209a3fe14ad0b1bd478018f19878f4b8c74f))
* **deps:** update dependency primelocale to v1.2.3 ([0be78df](https://gitlab.com/appframework/fairplanner-web/commit/0be78df5cb5c6028708c0f8f4ab9b67df3b61769))

## [1.2.22](https://gitlab.com/appframework/fairplanner-web/compare/1.2.21...1.2.22) (2025-01-01)

## [1.2.21](https://gitlab.com/appframework/fairplanner-web/compare/1.2.20...1.2.21) (2024-12-25)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.18 ([69bc617](https://gitlab.com/appframework/fairplanner-web/commit/69bc617f303c906fbec8511e5cd566ede1eef2c4))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.4.final ([d56ab7f](https://gitlab.com/appframework/fairplanner-web/commit/d56ab7fa6babaffebabe03d774cbbaa61697eb3e))
* **deps:** update quarkus.platform.version to v3.17.5 ([dd7f8be](https://gitlab.com/appframework/fairplanner-web/commit/dd7f8be2c4f17e19873e9b2584da7a5ae5d303d1))

## [1.2.20](https://gitlab.com/appframework/fairplanner-web/compare/1.2.19...1.2.20) (2024-12-18)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.15 ([da15c93](https://gitlab.com/appframework/fairplanner-web/commit/da15c93957e0517251da4a5f95f3e57e05c75848))
* **deps:** update dependency org.apache.logging.log4j:log4j-to-slf4j to v2.24.3 ([9d86cee](https://gitlab.com/appframework/fairplanner-web/commit/9d86cee6d932243a650b94100480f5cfcaf880aa))

## [1.2.19](https://gitlab.com/appframework/fairplanner-web/compare/1.2.18...1.2.19) (2024-12-13)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.14 ([8868bc5](https://gitlab.com/appframework/fairplanner-web/commit/8868bc55a9143dbab782fd1fd487674afac3139a))

## [1.2.18](https://gitlab.com/appframework/fairplanner-web/compare/1.2.17...1.2.18) (2024-12-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12 ([7a0a2cf](https://gitlab.com/appframework/fairplanner-web/commit/7a0a2cf24f662ec68da3cc6b5f522789d19f8117))

## [1.2.17](https://gitlab.com/appframework/fairplanner-web/compare/1.2.16...1.2.17) (2024-12-11)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.11 ([8671a29](https://gitlab.com/appframework/fairplanner-web/commit/8671a29dc99a37d464a098b39402ed0cf8f3bbae))

## [1.2.16](https://gitlab.com/appframework/fairplanner-web/compare/1.2.15...1.2.16) (2024-12-06)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v11.3.0 ([716394d](https://gitlab.com/appframework/fairplanner-web/commit/716394d580aa3358a2ed9af3888b6e7ebe751ce2))
* **deps:** update dependency vue-router to v4.5.0 ([3f91968](https://gitlab.com/appframework/fairplanner-web/commit/3f91968b8a7ebe6de85fd457d5a7a0cb8f8d8236))

## [1.2.15](https://gitlab.com/appframework/fairplanner-web/compare/1.2.14...1.2.15) (2024-12-05)

## [1.2.14](https://gitlab.com/appframework/fairplanner-web/compare/1.2.13...1.2.14) (2024-12-05)


### Bug Fixes

* **deps:** update dependency vue-i18n to v10.0.5 ([c5cf7fc](https://gitlab.com/appframework/fairplanner-web/commit/c5cf7fcfc720bffbbd4560f4be189b6bd4cb9d24))

## [1.2.13](https://gitlab.com/appframework/fairplanner-web/compare/1.2.12...1.2.13) (2024-12-04)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.8 ([1de7deb](https://gitlab.com/appframework/fairplanner-web/commit/1de7deb71ff890c03c8056ae7e9fad0bb4d82277))

## [1.2.12](https://gitlab.com/appframework/fairplanner-web/compare/1.2.11...1.2.12) (2024-11-27)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.5 ([e439cfc](https://gitlab.com/appframework/fairplanner-web/commit/e439cfc2405db4c201a469a4c8bc02cd48eb2b12))
* **deps:** update dependency org.apache.logging.log4j:log4j-to-slf4j to v2.24.2 ([3f9f246](https://gitlab.com/appframework/fairplanner-web/commit/3f9f246d7fd2294a839b6172495a1a9083d2658b))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.3.final ([52f751c](https://gitlab.com/appframework/fairplanner-web/commit/52f751cc03fac9b8246a0b4553c77ceee7573805))
* **deps:** update dependency primelocale to v1.2.2 ([34010fa](https://gitlab.com/appframework/fairplanner-web/commit/34010fa4091a2d7daed323bbeb34174ed9b0a259))

## [1.2.11](https://gitlab.com/appframework/fairplanner-web/compare/1.2.10...1.2.11) (2024-11-20)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to ^0.26.0 ([6d635e3](https://gitlab.com/appframework/fairplanner-web/commit/6d635e3fbd441b01315cb839402b452b202c243e))
* **deps:** update dependency org.geotools:gt-shapefile to v32.1 ([53e05d2](https://gitlab.com/appframework/fairplanner-web/commit/53e05d2b3bf9baecdae96771bd27c1ff512e13af))
* **deps:** update dependency primelocale to v1.2.0 ([8b8c77f](https://gitlab.com/appframework/fairplanner-web/commit/8b8c77fb7cf2eb33f616b03a1180a7c6c654ec6a))
* **deps:** update dependency vue to v3.5.13 ([88fd45d](https://gitlab.com/appframework/fairplanner-web/commit/88fd45de3242ebaa7aeec758e6b229f5251e1228))

## [1.2.10](https://gitlab.com/appframework/fairplanner-web/compare/1.2.9...1.2.10) (2024-11-15)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.14 ([beb1e5a](https://gitlab.com/appframework/fairplanner-web/commit/beb1e5a945a679e7ac51d18163f437d0362f4bf9))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.2.final ([95ca3c5](https://gitlab.com/appframework/fairplanner-web/commit/95ca3c535009f112660dc08c8619a62bf802e28f))
* **deps:** update dependency primelocale to v1.1.1 ([7d86df1](https://gitlab.com/appframework/fairplanner-web/commit/7d86df195e3d08130c9887a0ef7fd9982f204f77))

## [1.2.9](https://gitlab.com/appframework/fairplanner-web/compare/1.2.8...1.2.9) (2024-11-15)

## [1.2.8](https://gitlab.com/appframework/fairplanner-web/compare/1.2.7...1.2.8) (2024-11-14)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.13 ([4f6617b](https://gitlab.com/appframework/fairplanner-web/commit/4f6617bbb654ff5c5d12491792c9fc98dc639bff))

## [1.2.7](https://gitlab.com/appframework/fairplanner-web/compare/1.2.6...1.2.7) (2024-11-06)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v11.2.0 ([3d9a4d3](https://gitlab.com/appframework/fairplanner-web/commit/3d9a4d3018f9ce6e165207c84fa70d59bb179d19))

## [1.2.6](https://gitlab.com/appframework/fairplanner-web/compare/1.2.5...1.2.6) (2024-10-29)

## [1.2.5](https://gitlab.com/appframework/fairplanner-web/compare/1.2.4...1.2.5) (2024-10-29)

## [1.2.4](https://gitlab.com/appframework/fairplanner-web/compare/1.2.3...1.2.4) (2024-10-28)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.10 ([0d0ae1d](https://gitlab.com/appframework/fairplanner-web/commit/0d0ae1de19756c1f499623b3f13e36e95227adb8))

## [1.2.3](https://gitlab.com/appframework/fairplanner-web/compare/1.2.2...1.2.3) (2024-10-25)

## [1.2.2](https://gitlab.com/appframework/fairplanner-web/compare/1.2.1...1.2.2) (2024-10-23)

## [1.2.1](https://gitlab.com/appframework/fairplanner-web/compare/1.2.0...1.2.1) (2024-10-22)

# [1.2.0](https://gitlab.com/appframework/fairplanner-web/compare/1.1.2...1.2.0) (2024-10-22)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.7 ([3fc3b17](https://gitlab.com/appframework/fairplanner-web/commit/3fc3b17886a530ece275daca23c53ae8314f0225))
* **deps:** update dependency primelocale to v1.0.4 ([ed1c794](https://gitlab.com/appframework/fairplanner-web/commit/ed1c794f63627538c6f5bd94897b8fef048135f6))
* Fetch customers and regions during load ([55631a2](https://gitlab.com/appframework/fairplanner-web/commit/55631a2e308026c80374ea336c13a5f60785ec77))


### Features

* Add ability to manually geocode the customers ([7c2b4b5](https://gitlab.com/appframework/fairplanner-web/commit/7c2b4b5c8c79a6fbe6fd3fc530bacf19172be23b))

## [1.1.2](https://gitlab.com/appframework/fairplanner-web/compare/1.1.1...1.1.2) (2024-10-17)

## [1.1.1](https://gitlab.com/appframework/fairplanner-web/compare/1.1.0...1.1.1) (2024-10-17)


### Bug Fixes

* Add missing BinaryDataService ([730ad77](https://gitlab.com/appframework/fairplanner-web/commit/730ad775f78817ebfa679ad13f29a16e0f3f1196))

# [1.1.0](https://gitlab.com/appframework/fairplanner-web/compare/1.0.2...1.1.0) (2024-10-17)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.2 ([2b9aa28](https://gitlab.com/appframework/fairplanner-web/commit/2b9aa28c99b83cf1fd7efd4417267056d3e8b478))


### Features

* Add alias to region groups ([465fa01](https://gitlab.com/appframework/fairplanner-web/commit/465fa01544e3036fb0c5e0e64ae5d7e2fd68d294))

## [1.0.2](https://gitlab.com/appframework/fairplanner-web/compare/1.0.1...1.0.2) (2024-10-16)


### Bug Fixes

* Fix tests after class renaming ([b466158](https://gitlab.com/appframework/fairplanner-web/commit/b466158b1a4c25ca9c6c40fa2f0ce3acf6aa35f5))

## [1.0.1](https://gitlab.com/appframework/fairplanner-web/compare/1.0.0...1.0.1) (2024-10-14)

# 1.0.0 (2024-10-13)


### Bug Fixes

* Add forgotten file ([cc95cc5](https://gitlab.com/appframework/fairplanner-web/commit/cc95cc50f3cf92585f66f0f9f6e445357c428d6c))
* **ci:** Reenable sonar ([7c2ff89](https://gitlab.com/appframework/fairplanner-web/commit/7c2ff899551d2b43e2e52067256373fec7a72c9a))
* **deps:** update dependency bootstrap-vue-next to ^0.25.0 ([0c357c3](https://gitlab.com/appframework/fairplanner-web/commit/0c357c3591774ad122f49e0c96c0da4a461ea2ab))
* **deps:** update dependency com.github.albfernandez:juniversalchardet to v2.5.0 ([6058f69](https://gitlab.com/appframework/fairplanner-web/commit/6058f6979a8055f4d85c8787e1b9309952990050))
* **deps:** update dependency io.quarkus:quarkus-bom to v3.10.0 ([ee6cf29](https://gitlab.com/appframework/fairplanner-web/commit/ee6cf2949189ef6bf0d1b9871b50f0b1c13143fb))
* **deps:** update dependency org.apache.commons:commons-csv to v1.11.0 ([7d46ec5](https://gitlab.com/appframework/fairplanner-web/commit/7d46ec5bd4b8da4263a0c8711e6477c0510c53bb))
* **deps:** update dependency org.apache.commons:commons-csv to v1.12.0 ([fc40201](https://gitlab.com/appframework/fairplanner-web/commit/fc402016749a3ffe011c763d91ecc4465be8263f))
* **deps:** update dependency org.apache.logging.log4j:log4j-to-slf4j to v2.24.0 ([1b6418f](https://gitlab.com/appframework/fairplanner-web/commit/1b6418f8d7dc2463225d0012be0b71fc542686b1))
* **deps:** update dependency org.apache.logging.log4j:log4j-to-slf4j to v2.24.1 ([97ef1dd](https://gitlab.com/appframework/fairplanner-web/commit/97ef1dd9e9f35d1318c05bbaebe7a8d6003294f2))
* **deps:** update dependency org.apache.poi:poi-ooxml to v5.3.0 ([81585ad](https://gitlab.com/appframework/fairplanner-web/commit/81585ad7042603be4309375ccf57b473f83f3004))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.4.4.final ([1726a44](https://gitlab.com/appframework/fairplanner-web/commit/1726a4450fc09b1d4508b04716ae78a2d21dc0f2))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.5.0.final ([baf4762](https://gitlab.com/appframework/fairplanner-web/commit/baf476228662ea02958d81f58941814f96c89273))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.5.2.final ([bb38302](https://gitlab.com/appframework/fairplanner-web/commit/bb38302a5525e05aa95438ce9d9f58a586967dd8))
* **deps:** update dependency org.hibernate.orm:hibernate-spatial to v6.6.1.final ([4d556e4](https://gitlab.com/appframework/fairplanner-web/commit/4d556e4cb6bc4ff816557e15ba8776c8d65a27ca))
* **deps:** update dependency org.openstreetmap.osmosis:osmosis-pbf2 to v0.49.2 ([e933dbb](https://gitlab.com/appframework/fairplanner-web/commit/e933dbb20af3eab81d173938b9868b508629ed42))
* **deps:** update dependency org.webjars.npm:angular-translate to v2.19.1 ([d2d924c](https://gitlab.com/appframework/fairplanner-web/commit/d2d924cf18851eadceb784905c7fd00c9e2368f4))
* **deps:** update dependency org.webjars.npm:angular-translate-loader-static-files to v2.19.1 ([5dc51f7](https://gitlab.com/appframework/fairplanner-web/commit/5dc51f7e26b328ea361839ebfa1cc1a1219c1754))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.11 ([bdc8c96](https://gitlab.com/appframework/fairplanner-web/commit/bdc8c960f3dc896a587ba086e302e7e1517efe52))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.12 ([8854e35](https://gitlab.com/appframework/fairplanner-web/commit/8854e359b23187c34cbd91cb9effa19e442280ae))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.13 ([85dae3f](https://gitlab.com/appframework/fairplanner-web/commit/85dae3f22ca5a5032fed8bb0ab76dbff5b71f948))
* **deps:** update dependency org.webjars.npm:leaflet to v1.9.4 ([546a31d](https://gitlab.com/appframework/fairplanner-web/commit/546a31d32131bc1e053ecf8338e98542b13134fb))
* **deps:** update dependency org.webjars.npm:tinycolor2 to v1.6.0 ([46353c6](https://gitlab.com/appframework/fairplanner-web/commit/46353c699581f6d03a9de5aaba0fe5d230774ec0))
* **deps:** update dependency primevue to v4.1.0 ([41bcc73](https://gitlab.com/appframework/fairplanner-web/commit/41bcc73426d5383da3ef2321c101e309122e4ddc))
* **deps:** update dependency vue-i18n to v10.0.4 ([71d369b](https://gitlab.com/appframework/fairplanner-web/commit/71d369b4d66144b8804912095489d0cf615c3049))


### Features

* Update to latest appframework ([7af39fa](https://gitlab.com/appframework/fairplanner-web/commit/7af39fa0a01df1deae1858615915587094e1194f))
* Update to latest appframework SNAPSHOT ([6046f64](https://gitlab.com/appframework/fairplanner-web/commit/6046f64ae4ac7f663ee0a89730d844c8ba4998ab))
